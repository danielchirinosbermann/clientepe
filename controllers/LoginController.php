<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use app\models\_Login;
use yii\web\Controller;
use app\models\Usuarios;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;

class LoginController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public $layout = false;

    public function actionIndex() {
        $model = new _Login();

        if ((Yii::$app->request->isAjax) && ($model->load(Yii::$app->request->post()))) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {

                $usuario = Usuarios::findOne(["usuario" => $model->Usuario, "clave" => $model->Clave]);

                if ($usuario != null) {

                    $session = Yii::$app->session;
                    $session->open();
                    $session['IdUsuario'] = $usuario->id;
                    $session['nombreUsuario'] = $usuario->usuario;
                    Yii::$app->session->set('dct_user_id',$usuario->id);
                    Yii::$app->session->set('dct_user_name',$usuario->usuario);
                    $loginDCT = Yii::$app->runAction('dct/login');
                    if (isset($loginDCT->auth)) {
                        $session['tokenDCT'] = $loginDCT->auth;
                        Yii::$app->session->set('dct_token',$loginDCT->auth);
                    } else {
                        echo "error de conexion al api, mensage: " . $loginDCT;
                        exit;
                    }
                    return $this->redirect(['site/index']);
                } else {
                    $noLogin = 1;
                    return $this->render('login', ['model' => $model, "noLogin" => $noLogin]);
                }
            } else {
                $model->getErrors();
            }
        } else {
            return $this->render('login', ['model' => $model]);
        }
    }

    public function actionCerrarsesion() {
        $session = Yii::$app->session;
        foreach ($session as $name => $value) {
            $session->remove($name);
        }
        $session->close();
        return $this->redirect(['/login']);
    }

}
