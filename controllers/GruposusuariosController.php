<?php

namespace app\controllers;

use Yii;
use stdClass;
use yii\web\Response;
use yii\web\Controller;
use app\models\Usuarios;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\models\GruposDctUsuarios;

class GruposusuariosController extends Controller{

    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action) {
        
        if ($this->action->id == "error"){
            return $this->render('error');
        }

        if ($action->actionMethod == "actionGetgruposporusuario" ) { 
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }


    public function init() {

        if (!isset(Yii::$app->session["nombreUsuario"])) {
            return $this->redirect(['/login']);
        }
    }


    public function actionCrear(){

        $session = Yii::$app->session;

        $model = new GruposDctUsuarios();

        if ((Yii::$app->request->isAjax) && ($model->load(Yii::$app->request->post()))) {
			
            Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

        if ($_POST) {
            if (isset($_POST["GruposDctUsuarios"]["id_grupo_dct"]) && isset($_POST["GruposDctUsuarios"]["id_usuario"])) {
                if ($_POST["GruposDctUsuarios"]["id_usuario"] != "") {

                    $GruposDeUsuario = GruposDctUsuarios::find()->where(["id_usuario" => $_POST["GruposDctUsuarios"]["id_usuario"]])->all();

                    if (count($GruposDeUsuario) > 0) {
                        foreach ($GruposDeUsuario as $key => $value) {
                            $value->delete();
                        }
                    }

                    foreach ($_POST["GruposDctUsuarios"]["id_grupo_dct"] as $key => $value) {
                        $model = new GruposDctUsuarios();
                        $model->id_grupo_dct = $value;
                        $model->id_usuario = $_POST["GruposDctUsuarios"]["id_usuario"];
                        $model->save();
                    }
                }
            }   
            
            return $this->redirect(["crear"]);
        }

        $usuarios = ArrayHelper::map(Usuarios::find()->where(['<>','id', 1])->all(), 'id', 'usuario');

        $response = Yii::$app->runAction('dct/grupos', ["token" => $session['tokenDCT']]);

        $grupos = [];
        if (count($response) > 0) {
            foreach ($response as $key => $value) {
                $grupos[$value->id] = $value->name;
            }
        }
        
        return $this->render('crear', ["model" => $model, "usuarios" => $usuarios, "grupos" => $grupos]);
    }

    public function actionEditar($id){

        $session = Yii::$app->session;

        $model = new GruposDctUsuarios();

        if ((Yii::$app->request->isAjax) && ($model->load(Yii::$app->request->post()))) {
			
            Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

        if ($_POST) {

            if (isset($_POST["GruposDctUsuarios"]["id_grupo_dct"])) {

                    $GruposDeUsuario = GruposDctUsuarios::find()->where(["id_usuario" => $id])->all();

                    if (count($GruposDeUsuario) > 0) {
                        foreach ($GruposDeUsuario as $key => $value) {
                            $value->delete();
                        }
                    }

                    foreach ($_POST["GruposDctUsuarios"]["id_grupo_dct"] as $key => $value) {
                        $model = new GruposDctUsuarios();
                        $model->id_grupo_dct = $value;
                        $model->id_usuario = $id;
                        $model->save();
                    }

            }   
            
            return $this->redirect(["lista"]);
        }

        $usuario = Usuarios::findOne($id);

        $response = Yii::$app->runAction('dct/grupos', ["token" => $session['tokenDCT']]);

        $grupos = [];
        if (count($response) > 0) {
            foreach ($response as $key => $value) {
                $grupos[$value->id] = $value->name;
            }
        }
        
        return $this->render('editar', ["model" => $model, "usuario" => $usuario, "grupos" => $grupos, "idUsuario" => $id]);
    }

    public function actionLista(){

        $db = Yii::$app->db;
        $sql = $db->createCommand("SELECT id_usuario, u.usuario, count(id_grupo_dct) as cantidad_grupos from grupos_dct_usuarios gu inner join usuarios u on gu.id_usuario = u.id
        group by id_usuario, u.usuario");

        $gruposusarios = $sql->queryAll();
        
        return $this->render('lista', ["gruposusarios" => $gruposusarios]);
    }
    
    public function actionGetgruposporusuario(){
        $res = array();
        if (isset($_POST["id"])) {
            $idUsuario = $_POST["id"];
            $gruposPorUsuario = GruposDctUsuarios::find()->where(["id_usuario" => $idUsuario ])->asArray()->all();
            if ($gruposPorUsuario!=false){
                foreach($gruposPorUsuario as $grupo){
                  $res[] = $grupo['id_grupo_dct']; 
                }
            }
        }
        return implode(',',$res);
    }

}
