<?php

namespace app\controllers;

use Yii;
use stdClass;
use app\models\Users;
use yii\web\Response;
use yii\web\Controller;
use app\models\Usuarios;
use app\models\UserSensor;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\models\SensorTemperaturaSt;
use app\models\AgregarSensorUsuario;

class UsuariosController extends Controller{

    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    // public function beforeAction($action) {
    //     if ($this->action->id == "error"){
    //         return $this->render('error');
    //     }
    //     if ($action->actionMethod == "actionGettemperatura" || $action->actionMethod == "actionGetdetalletemperatura"  || $action->actionMethod == "actionGetmac") {
    //         $this->enableCsrfValidation = false;
    //     }
    //     return parent::beforeAction($action);
    // }

    public function init() {

        if (!isset(Yii::$app->session["nombreUsuario"])) {
            return $this->redirect(['/login']);
        }
    }

    public function actionCrear(){

        $model = new Usuarios();

        if ((Yii::$app->request->isAjax) && ($model->load(Yii::$app->request->post()))) {
			
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
        }
        
        if ($_POST){

            $model->usuario = $_POST["Usuarios"]["usuario"];
            $model->clave = $_POST["Usuarios"]["clave"];

            if ($model->save()) {
                return $this->redirect(["lista"]);
            }else{
                echo "error";
                exit;
            }
            

        }
        return $this->render('crear', ["model" => $model]);
    }

    public function actionLista(){

        $usuarios = Usuarios::find()->where(["<>","id", 1])->all();

        return $this->render('lista',["usuarios" => $usuarios]);
    }


    public function actionEditar($id){
        $model = Usuarios::findOne($id);

		if ((Yii::$app->request->isAjax) && ($model->load(Yii::$app->request->post()))) {
			
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
        }
        
        if ($_POST) {
			if ($model->validate()) {                

                $model->usuario = $_POST["Usuarios"]["usuario"];
                $model->clave = $_POST["Usuarios"]["clave"];
               
                if ($model->save()) {
                       
                    
                }else{
                   
                }

                return $this->redirect(['lista']);
                
	
			} else{
                return $this->redirect(['lista']);

			}
		}else{
			return $this->render('editar', ['model' => $model]);
		}

        return $this->render('editar');
    }

}
