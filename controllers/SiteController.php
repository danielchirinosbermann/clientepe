<?php

namespace app\controllers;

use Yii;
use stdClass;
use yii\web\Response;
use yii\web\Controller;
use app\models\Usuarios;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\models\GruposDctUsuarios;
use yii\helpers\Html;

class SiteController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    // public function beforeAction($action) {
    //     if ($action->actionMethod == "actionGettemperatura" || $action->actionMethod == "actionGetdetalletemperatura"  || $action->actionMethod == "actionGetmac" || $action->actionMethod == "actionGetsensor") {
    //         $this->enableCsrfValidation = false;
    //     }
    //     return parent::beforeAction($action);
    // }

    public function init() {

        if (!isset(Yii::$app->session["nombreUsuario"])) {
            return $this->redirect(['/login']);
        }
    }

    public function actionVehiculosGrupo() {
        $grupo = isset($_POST['grupo']) && $_POST['grupo'] > 0 ? $_POST['grupo'] : 0;
        $res = "";
        if ($grupo > 0) {
            $res = Yii::$app->engine->getVehiculosPorgrupoCSV($grupo);
        }
        echo $res;
    }

    public function actionIndex() {
        $session = Yii::$app->session;
        if ($session["IdUsuario"] == 1) {
            return $this->redirect(['gruposusuarios/lista']);
        }
        return $this->render('index', ["vehiculos" => []]);
    }

    public function actionGetUlpos() {
        $session = Yii::$app->session;
        $GruposDeUsuario = GruposDctUsuarios::find()->where(["id_usuario" => $session["IdUsuario"]])->all();
        $vehiculosID = [];
        if (count($GruposDeUsuario) > 0) {
            foreach ($GruposDeUsuario as $key => $value) {
                $grupoDetalle = Yii::$app->runAction('dct/vehiculosporgrupo', ["id" => $value["id_grupo_dct"], "token" => $session['tokenDCT']]);
                if (count($grupoDetalle) > 0) {
                    foreach ($grupoDetalle as $kv => $vv) {
                        $vehiculosID[] = $vv;
                    }
                }
            }
        }
        $ultpos = [];
        if (count($vehiculosID) > 0) {
            $ultpos = Yii::$app->runAction('dct/ultposvehiculos', ["vehiculos" => implode(",", $vehiculosID), "token" => $session['tokenDCT']]);
        }
        if (is_array($ultpos) && count($ultpos) == 0) {
            return $this->redirect(['/login']);
        }
        return json_encode($ultpos);
    }

    public function actionGetGrupos() {
        $session = Yii::$app->session;
        $mostrar = isset($_POST['mostrar']) == true && $_POST['mostrar'] > 0 ? $_POST['mostrar'] : 1;
        $res = '<div id="listado_grupos" class="panel-group collapse in" aria-expanded="true" style="padding-right:5px;">';
        $session = Yii::$app->session;
        $GruposDeUsuario = GruposDctUsuarios::find()->where(["id_usuario" => $session["IdUsuario"]])->all();
        $vehiculosID = [];
        $grupos = '';
        $gruposArray = [];
        $UTC = new \DateTimeZone("UTC");
        if (count($GruposDeUsuario) > 0) {
            $contador = 0;
            $dataGrupos = Yii::$app->engine->getListadoGrupos();
            $data = [];
            foreach ($GruposDeUsuario as $key => $value) {
                $grupoId = $value["id_grupo_dct"];
                // $grupoName = isset($dataGrupos[$grupoId]) == true ? $dataGrupos[$grupoId] : "";
                $grupoName = '';
                $fechaEvento = '';
                $grupoDetalle = Yii::$app->runAction('dct/vehiculosporgrupo', ["id" => $grupoId, "token" => $session['tokenDCT']]);
                if (count($grupoDetalle) > 0) {
                    foreach ($grupoDetalle as $kv => $vv) {
                        $vehiculosID[] = $vv;
                    }
                }
                if (count($vehiculosID) > 0) {
                    $data = Yii::$app->runAction('dct/ultposvehiculos', ["vehiculos" => implode(",", $vehiculosID), "token" => $session['tokenDCT']]);
                }

                $imagenFlechaVerde = '<img src="' . Yii::getAlias('@web') . '/images/flecha_verde.png" style="width:16px;height:16px;" />';
                $imagenFlechaAmarilla = '<img src="' . Yii::getAlias('@web') . '/images/flecha_amarilla.png" style="width:16px;height:16px;" />';
                $imagenFlechaRoja = '<img src="' . Yii::getAlias('@web') . '/images/flecha_roja.png" style="width:16px;height:16px;" />';
                $imagenFlechaStop = '<img src="' . Yii::getAlias('@web') . '/images/stop_1.png" style="width:16px;height:16px;" />';
                $imagenFlechaStop2 = '<img src="' . Yii::getAlias('@web') . '/images/stop_2.png" style="width:16px;height:16px;" title="Vehiculo sin información" />';
                $imagenFlechaStop3 = '<img src="' . Yii::getAlias('@web') . '/images/stop_3.png" style="width:16px;height:16px;" title="" />';
                $imagenFlecha = $imagenFlechaVerde;
                // lleno el arreglo de patentes por alias 
                $patentesArray = array();
                if (isset($data) == true && is_array($data) == true && count($data) > 0) {
                    foreach ($data as $patente) {
                        $patentesArray[$patente->id] = $patente;
                        if (isset($patente->info->alias) == true) {
                            $alias = $patente->info->alias;
                            $gruposArray[$alias][] = $patente->id;
                        } else {
                            $gruposArray['Otros'][] = $patente->id;
                        }
                    }
                }
                // ordeno el array por alias alfabeticamente
                ksort($gruposArray);
                if (count($gruposArray) > 0) {
                    foreach ($gruposArray as $key => $value) {
                        $aliasName = $key;
                        if (is_array($value) == true && count($value) > 0) {
                            $cantidadPatentes = count($value);
                            $patentesSort = array();
                            $patentesFechasArray = array();
                            $flechas = array();
                            $grupoName = $aliasName . ' ( ' . $cantidadPatentes . ' )';
                            foreach ($value as $patenteId) {
                                $fechaUltpos = '';
                                $dataPatente = $patentesArray[$patenteId];

                                // guardando la fecha del ulpos
                                if (isset($dataPatente->device->latest->loc) == true) {
                                    $vArray = $dataPatente->device->latest->loc;
                                    $tiempo = isset($vArray->evtime) == true ? $vArray->evtime : "";
                                    if ($tiempo != "") {
                                        $fechaEvento_1 = date('d/m', $tiempo);
                                        $fechaEvento_2 = date('H:i', $tiempo);
                                        $fechaUltpos = $fechaEvento_1 . ' ' . $fechaEvento_2;
                                        $patentesFechasArray[$patenteId] = $fechaEvento_1 . ' ' . $fechaEvento_2;
                                        $fechaEvento = $fechaEvento_1 . ' ' . $fechaEvento_2;
                                    }
                                }
                                $imagenFlecha = $imagenFlechaVerde;
                                $velocidad = isset($dataPatente->device->latest->loc->mph) == true ? $dataPatente->device->latest->loc->mph : 0;
                                if (isset($dataPatente->device->latest->data->io_ign) == true) {
                                    $tiempo_detenido_minutos = '';
                                    if ($dataPatente->device->latest->data->io_ign->value == false){
                                        $datoprevio = $dataPatente->device->latest->data->io_ign->change->evtime;
                                        $datoactual = $dataPatente->device->latest->data->io_ign->evtime;
                                        $fechaPrevia = date('Y-m-d H:i:s', $datoprevio);
                                        $fechaActual = date('Y-m-d H:i:s', $datoactual);
                                        $milliseconds = abs($datoactual - $datoprevio);
                                        $seconds = floor($milliseconds / 1000);
                                        $tiempo_detenido_minutos = floor($seconds / 60);
                                    }
                                    
                                    if ($tiempo_detenido_minutos > 0 && $velocidad < 5) {
                                        
                                        if ($tiempo_detenido_minutos > 0 && $tiempo_detenido_minutos <= 5 && $velocidad < 5) {
                                            $imagenFlecha = $imagenFlechaAmarilla;
                                        }
                                        if ($tiempo_detenido_minutos > 5 && $tiempo_detenido_minutos <= 25 && $velocidad < 5) {
                                            $imagenFlecha = $imagenFlechaRoja;
                                        }
                                        if ($tiempo_detenido_minutos > 25 && $tiempo_detenido_minutos <= 2900 && $velocidad < 5) {
                                            $imagenFlecha = $imagenFlechaStop;
                                        }
                                        if ($tiempo_detenido_minutos > 2880 && $velocidad < 5) {
                                            $imagenFlecha = $imagenFlechaStop3;
                                        }
                                    }
                                }
                                if ($velocidad > 5){
                                    $imagenFlecha = $imagenFlechaVerde;
                                }
                                if ($fechaEvento == ''){
                                   $imagenFlecha = $imagenFlechaStop2; 
                                }
                                
                                
                                $flechas[$patenteId] = $imagenFlecha;
                                $patenteName = isset($dataPatente->info) == true ? $dataPatente->info->license_plate : "";
                                $muestra = isset($dataPatente->name) ? $dataPatente->name : "";
                                $id = $dataPatente->id;
                                $nombre = $mostrar == 1 ? $patenteName : $muestra;
                                $patentesSort[$patenteId] = $nombre;
                            } // fin de patentes dentro del grupo

                            asort($patentesSort);
                            $patentes = '';
                            if (count($patentesSort) > 0) {
                                foreach ($patentesSort as $key => $item) {
                                    $itemNameStr = substr($item, 0, 20);
                                    $itemName = strlen($itemNameStr) == 20 ? $itemNameStr . '..' : $itemNameStr;
                                    $fechaValor = isset($patentesFechasArray[$key]) == true ? $patentesFechasArray[$key] : "";
                                    $flechaImagen = isset($flechas[$key]) == true ? $flechas[$key] : "";
                                    $nombreLabel = '<div class="itemlista_nombre"><a name="p_' . $key . '">&nbsp;</a><span id="nombre_' . $key . '" title="' . $item . '"  onclick="muestraVehiculoSinPos(' . $key . ');"  class="btn btn-default btn-xs nombreclase" style="width:100%;text-align:left;border:none;margin:3px;">' . $itemName . '</span></div>';
                                    if ($fechaValor != "") {
                                        $nombreLabel = '<div class="itemlista_nombre"><a name="p_' . $key . '">&nbsp;</a><span id="nombre_' . $key . '" onclick="muestraVehiculo(' . $key . ');" title="' . $item . '" class="btn btn-default btn-xs nombreclase" style="width:100%;text-align:left;border:none;margin:3px;color:blue">' . $itemName . '</span></div>';
                                    }
                                    $comandos = '<span onclick="enviaComandos(' . $key . ');" title="enviar comandos" style="margin-left:15px;cursor:pointer"><i class="fa fa-hand-paper"></i></span>';
                                    $refrescar = '<span id="forzar_' . $key . '" onclick="enviaRefresh(' . $key . ');" title="enviar peticion de registro" style="margin-left:15px;cursor:pointer"><i style="color:green" class="fa fa-sync-alt"></i></span>';
                                    // $flecha = '<span data-patente="' . $key . '" class="patente_icon" style="margin-left:5px;cursor:pointer">' . $flechaImagen . '</span>';
                                    $flecha = '<span data-patente="' . $key . '" class="patente_icon" style="margin-left:5px;cursor:pointer"></span>';
                                    $fecha = '<span data-patente="' . $key . '" class="patente_fecha" style="font-size:11px;padding-bottom:2px;">' . $fechaValor . '</span>';
                                    $botones = '<div class="itemlista_icon">' . $flecha . $comandos .$refrescar. '</div>';
                                    $fechaDiv = '<div class="itemlista_fecha">' . $fecha . '</div>';
                                    $clear = '<div style="float:none">&nbsp;</div>';
                                    $line = '<div style="height:30px;font-size:12px;margin-bottom:5px;border-bottom:0.1em solid lightgray">' . $nombreLabel . $fechaDiv . $botones . '</div>';
                                    $patentes = $patentes . $line;
                                }
                            }
                            $grupos = $grupos . '<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="collapse-toggle collapsed" href="#listado_grupos-collapse' . $contador . '" data-toggle="collapse" data-parent="#listado_grupos" aria-expanded="false"><i class="fa fa-users"></i> ' . $grupoName . '</a>
                                    </h4>
                                </div>
                                <div id="listado_grupos-collapse' . $contador . '" class="panel-collapse collapse" style="">
                                    <div class="panel-body">
                                        ' . $patentes . '
                                    </div>
                                </div>
                            </div>';
                            $contador++;
                        } // contador de patentes dentro del grupo 
                    }
                }
            }
        }

        return $res . $grupos . '</div>';
    }

    public function actionEnviaComando() {
        $id = isset($_POST['id']) == true && $_POST['id'] > 0 ? $_POST['id'] : "";
        $comando = isset($_POST['comando']) == true && $_POST['comando'] != "" ? $_POST['comando'] : "";
        if ($id == "" || $comando == "") {
            return 'ERROR PARAMETROS';
        }
        $data = array(
            'cmd' => $comando,
            'includeImei' => true
        );
        $payload = json_encode($data);
        ini_set("curl.cainfo", null);
        $data = array();
        $curl = curl_init();
        $token = Yii::$app->session->get('dct_token');
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dct.bermanngps.cl/api/vehicles/" . $id . "/remote/console",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $payload,
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                'Content-Type: application/json',
                'Content-Length: ' . strlen($payload),
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: dct.bermanngps.cl",
                "Authenticate: " . $token,
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $data = "Error #:" . $err;
        } else {
            $data = $response;
        }
        return $data;
    }
    
    public function actionEsperaResponse() {
        $id = isset($_POST['id']) == true && $_POST['id'] > 0 ? $_POST['id'] : "";
        $cid = isset($_POST['cid']) == true && $_POST['cid'] > 0 ? $_POST['cid'] : "";
        if ($id == "" || $cid == "") {
            return 'ERROR PARAMETROS';
        }
        ini_set("curl.cainfo", null);
        $data = array();
        $curl = curl_init();
        $token = Yii::$app->session->get('dct_token');
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dct.bermanngps.cl/api/vehicles/" . $id . "/remote/console?cid=".$cid,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: dct.bermanngps.cl",
                "Authenticate: " . $token,
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $data = "Error #:" . $err;
        } else {
            $data = $response;
        }
        return $data;
    }
    

}
