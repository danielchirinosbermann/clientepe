<?php

namespace app\controllers;

use Yii;
use DateTime;
use stdClass;
use DateTimeZone;
use yii\web\Response;
use yii\web\Controller;
use app\models\Usuarios;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\models\FechasVehiculos;
use app\models\GruposDctUsuarios;

class ReportesController extends Controller{

    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /*public function beforeAction($action) {
        if ($action->actionMethod == "actionGetdatoshistorico") {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }*/

    public function init() {

        if (!isset(Yii::$app->session["nombreUsuario"])) {
            return $this->redirect(['/login']);
        }
    }

    public function actionHistorico(){
        return $this->render('historico');
    }
    
    public function actionAyuda(){
        return $this->render('ayuda');
    }
    
    public function actionFlota(){
        return $this->render('flota');
    }
    
    public function actionEventos(){
        $this->layout = '@app/views/layouts/popup';
        return $this->render('eventos');
    }
    
    public function actionMosaico(){
        $this->layout = '@app/views/layouts/popup';
        return $this->render('mosaico');
    }
    
    /*public function actionHistorico(){

        $session = Yii::$app->session;

        //se buscan los grupos que tiene el cliente
        $grupos = GruposDctUsuarios::find()->where(["id_usuario" => $session['IdUsuario']])->asArray()->all();

        // se viene al menos 1 grupo se guardan los ID en un nuevo arreglo para manipularlos
        if (count($grupos) > 0) {
            $gruposDeUsuario = [];
            foreach ($grupos as $key => $value) {
                $gruposDeUsuario[] =  intval($value["id_grupo_dct"]);
            }

            //se hace el llamado el api de los grupos para obtener los vehiculos
            $getGrupos = Yii::$app->runAction('dct/grupos', ["token" => $session['tokenDCT']]);

            $vehiculosPorGrupoDeUsuario = [];
            //se recorren todos los grupos de DCT
            foreach ($getGrupos as $ke => $kva) {
                //se recorren los grupos dct asignado al usuario de esta web
                foreach ($gruposDeUsuario as $k => $v) {
                    //si conincide el id del grupo para ese usuario 
                    if ($kva->id == $v) {
                        // y si tiene al menos 1 vehiculo
                        if (count($kva->vehicles) > 0) {
                            // se gardan los id de cada vehiculo para buscar el detalle
                            foreach ($kva->vehicles as $kk => $vv) {
                                $vehiculosPorGrupoDeUsuario[] = $vv;
                            }
                        }
                    }
                }
            }

            //por cada id de vehiculo se hace un llamado al api para buscar el detalle
            $vehiculoDetalle = [];
            foreach ($vehiculosPorGrupoDeUsuario as $kvg => $vvg) {
                $detalleVehiculo = Yii::$app->runAction('dct/detallevehiculo', ["id" => $vvg, "token" => $session['tokenDCT']]);

                //si el vehiculo tiene detalle se guarda solo el id y el nombre
                if ($detalleVehiculo != null) {
                    // $vehiculoDetalle[$kvg]["id"] = $detalleVehiculo->id;
                    // $vehiculoDetalle[$kvg]["nombre"] = $detalleVehiculo->name;
                    $vehiculoDetalle[$detalleVehiculo->id] = $detalleVehiculo->name;
                }
            }

            $model = new FechasVehiculos();
            return $this->render('historico', ["vehiculoDetalle" => $vehiculoDetalle, "model" => $model]);

        }else{
            echo "sin grupos para mostrar";exit;
            return $this->render('index');
        } 
        
    }
    */
    
    public function actionGetdatoshistorico(){

        $session = Yii::$app->session;

        $model = new FechasVehiculos();

        if ((Yii::$app->request->isAjax) && ($model->load(Yii::$app->request->post()))) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }


        if ($_POST) {
            //con el post del vehiculo buscamos el detalle del vehiculo (nombre) para agregarlo al historico

            $fechaHoraI = explode(" ",$_POST["FechasVehiculos"]["fechaInicio"]);
            $fechaI = explode("/",$fechaHoraI[0]);
            $horaI = explode(":",$fechaHoraI[1]);

            $fechaHoraF = explode(" ",$_POST["FechasVehiculos"]["fechaFin"]);
            $fechaF = explode("/",$fechaHoraF[0]);
            $horaF = explode(":",$fechaHoraF[1]);

            $fechaInicio = $fechaI[2]."-".$fechaI[1]."-".$fechaI[0]."T".$horaI[0].":".$horaI[1].":00";
            $fechaFin = $fechaF[2]."-".$fechaF[1]."-".$fechaF[0]."T".$horaF[0].":".$horaF[1].":00";
            $id_vehiculo_dct = $_POST["FechasVehiculos"]["vehiculos"];


            $model->fechaInicio = $_POST["FechasVehiculos"]["fechaInicio"];
            $model->fechaFin = $_POST["FechasVehiculos"]["fechaFin"];
            $model->vehiculos = $_POST["FechasVehiculos"]["vehiculos"];

            $vehiculoDetalle = json_decode($_POST["txt_vehiculo_detalle"]);


            $fI = date("Y-m-d H:i:00", strtotime(str_replace("T"," ",$fechaInicio. "+4 hours")));
            $fF = date("Y-m-d H:i:00", strtotime(str_replace("T"," ",$fechaFin. "+4 hours")));

            // echo $fechaInicio." ".$fI;exit;

            $fI = str_replace(" ","T",$fI);
            $fF = str_replace(" ","T",$fF);
            
            
            $response = Yii::$app->runAction('dct/historico', ["fechaInicio" => $fI, "fechaFin" => $fF, "vehiculos" => $id_vehiculo_dct, "token" => $session['tokenDCT']]);


            $db = Yii::$app->db;
            $sql = $db->createCommand("SELECT tts_v1 as id, teh.descripcion from ta_teh_standard_tts tts 
            inner join tipo_evento_homo_teh teh on teh.id = tts.teh_id 
            WHERE tts.tav_id in (1,3) limit 1");
            $eventos = $sql->queryAll();
            $historico = [];
            if (count($response) > 0) {
                foreach ($response as $key => $value) {
                    $historico[$key]["tiempo"] = $value->event_time;
                    foreach ($eventos as $k => $v) {

                        if ($value->code == $v["id"]) {
                            $historico[$key]["evento"] = $v["descripcion"];
                            break;
                        }
                    }
                    $historico[$key]["eventoid"] = $value->code;
                    $historico[$key]["vehiculo_id"] = $value->vid;
                    $historico[$key]["longitud"] = $value->lon;
                    $historico[$key]["latitud"] = $value->lat;
                    $historico[$key]["velocidad"] = $value->speed;
                }

            }
            return $this->render('historico', ["historico" => $historico, "vehiculoDetalle" => $vehiculoDetalle, "model" =>$model]);
        }
    }
    
    
    


}
