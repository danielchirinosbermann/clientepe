<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;

class DctController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    //hace login en el api
    public function actionLogin() {
        ini_set("curl.cainfo", null);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dct.bermanngps.cl/api/login",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n  \"username\": \"apidct@bermanngps.cl\",\n  \"password\": \"363combersaapidct\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Type: application/json",
                "Host: dct.bermanngps.cl",
                "cache-control: no-cache",
                "content-length: 75"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {

            return json_decode($response);
        }
    }

    //trae todos los grupos
    public function actionGrupos($token) {

        // $mensaje = $this->actionLogin();
        // var_dump($mensaje);exit;
        // if (isset($mensaje->auth)) {
        ini_set("curl.cainfo", null);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dct.bermanngps.cl/api/groups",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 200,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{\n  \"username\": \"apidct@bermanngps.cl\",\n  \"password\": \"363combersaapidct\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Type: application/json",
                "Host: dct.bermanngps.cl",
                "accept-encoding: gzip, deflate",
                "content-length: 75",
                "Authenticate: " . $token,
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $grupos = json_decode($response);
            return $grupos->data;
        }
    }

    //trae todos los vehiculos de un grupo
    public function actionVehiculosporgrupo($id, $token) {
        ini_set("curl.cainfo", null);
        $curl = curl_init();
      curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dct.bermanngps.cl/api/groups/$id",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{\n  \"username\": \"apidct@bermanngps.cl\",\n  \"password\": \"363combersaapidct\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Type: application/json",
                "Host: dct.bermanngps.cl",
                "accept-encoding: gzip, deflate",
                "content-length: 75",
                "Authenticate: " . $token,
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $vehiculos = json_decode($response);
            return isset($vehiculos->vehicles) == true ? $vehiculos->vehicles : [];
        }
    }

    //trae la ultima posicion de los vehiculos
    public function actionUltposvehiculos($vehiculos, $token) {

        // $mensaje = $this->actionLogin();
        // if (isset($mensaje->auth)) {
        ini_set("curl.cainfo", null);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dct.bermanngps.cl/api/vehicles?ids=$vehiculos&select=info,name,device:latest",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{\n  \"username\": \"apidct@bermanngps.cl\",\n  \"password\": \"363combersaapidct\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Type: application/json",
                "Host: dct.bermanngps.cl",
                "accept-encoding: gzip, deflate",
                "content-length: 75",
                "Authenticate: " . $token,
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        // echo '<pre>';
        // var_dump($response);
        // exit;
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $vehiculos = json_decode($response);

            return $vehiculos->data;
        }
    }

    //trae el detalle del vehiculo
    public function actionDetallevehiculo($id, $token) {

        // $mensaje = $this->actionLogin();
        // if (isset($mensaje->auth)) {
        ini_set("curl.cainfo", null);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dct.bermanngps.cl/api/vehicles/" . $id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: dct.bermanngps.cl",
                "Authenticate: " . $token,
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $vehiculos = json_decode($response);
            return $vehiculos;
        }
    }

    //trae el historico de un vehiculo entre fechas
    public function actionHistorico($fechaInicio, $fechaFin, $vehiculos, $token) {


        // $mensaje = $this->actionLogin();
        // if (isset($mensaje->auth)) {
        ini_set("curl.cainfo", null);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dct.bermanngps.cl/api/rawdata?from=$fechaInicio&to=$fechaFin&vehicles=$vehiculos&fields=code,lat,lon,speed",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Authenticate: " . $token,
                "Content-Type: application/json",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: dct.bermanngps.cl",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {

            $historico = json_decode($response);
            return $historico->events;
        }
        // }else{
        //     echo "error al iniciar sesion";
        //     exit;
        // }
    }

    // comandos felipe
    public function actionLogincomandos() {
        ini_set("curl.cainfo", null);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dct.bermanngps.cl/api/login",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n  \"username\": \"apidct@bermanngps.cl\",\n  \"password\": \"363combersaapidct\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Type: application/json",
                "Host: dct.bermanngps.cl",
                "cache-control: no-cache",
                "content-length: 75"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return "cURL Error #:" . $err;
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return json_decode($response);
        }
    }// fin comandos felipe
    
    
    
    
}
