<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\helpers\Html;
use codemix\yii2confload\Config;
use kartik\popover\PopoverX;
use yii\helpers\Url;

/*
 *  clase que sirve como repositorio de metodos para operaciones disponibles globalmente en toda la app 
 *  permite acceder a toda la app desde una opcion publica o privada
 */

class EngineComponent extends Component {
    /*
     * return id del usuario firmado en el sistema
     */

    var $api_rest1 = 'https://rest.bermanngps.cl:40001/BermannRest/api/';

    /*
     * listado de tipos de perfiles
     */

    public function listadoUsuarios() {
        $empresaId = $this->getEmpresaId();
        return ArrayHelper::map(\app\models\Users::find()->where(['empresa_id' => $empresaId])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name');
    }

    public function listadoTiposActividades() {
        return ArrayHelper::map(\app\models\TipoActividad::find()->orderBy(['nombre' => SORT_ASC])->all(), 'id', 'nombre');
    }

    /*
     * listado de tipos de perfiles
     */

    public function listadoTiposUsuarios() {
        if (Yii::$app->engine->getGroupId() == 1) {
            return ArrayHelper::map(\app\models\Groups::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'nombre');
        } else {
            return ArrayHelper::map(\app\models\Groups::find()->where('id > 1')->orderBy(['id' => SORT_ASC])->all(), 'id', 'nombre');
        }
    }

    public function listadoNeumaticos() {
        $empresaId = $this->getEmpresaId();
        return ArrayHelper::map(\app\models\Neumaticos::find()->where(['empresa_id' => $empresaId])->orderBy(['id_neumatico' => SORT_ASC])->all(), 'id', 'id_neumatico');
    }

    public function listadoEstadosUsuarios() {
        return array('1' => 'Habilitado', '2' => 'No Habilitado');
    }

    public function estadosNeumaticos() {
        return array('1' => 'Comprado', '2' => 'Montado', '3' => 'Desmontado');
    }

    public function listadoEstadosEgresos() {
        return array('1' => 'En Progreso', '2' => 'Completado');
    }

    public function listadoEstadosTraslados() {
        return array('1' => 'En Progreso', '2' => 'Completado');
    }

    /*
     * convierte una fecha en formato espanol a formato base de datos 
     */

    public function fechaSql($fecha) {
        if ($fecha != "") {
            $f = explode('/', $fecha);
            if (count($f) == 3) {
                $mes = strlen($f[1]) == 1 ? '0' . $f[1] : $f[1];
                $fecha = $f[2] . '-' . $mes . '-' . $f[0];
            }
        }
        return $fecha;
    }

    /*
     * valida el campo rut 
     */

    public function validateRut($rut) {
        $res = true;
        if ($rut != "") {
            $data = explode('-', $rut);
            if (strlen($rut) < 9 || strlen($rut) > 10 || count($data) != 2 || strlen($data[1]) != 1) {
                $res = false;
            }
            $data = explode('-', $rut);
            $evaluate = strrev($data[0]);
            $multiply = 2;
            $store = 0;
            for ($i = 0; $i < strlen($evaluate); $i++) {
                $store += $evaluate[$i] * $multiply;
                $multiply++;
                if ($multiply > 7)
                    $multiply = 2;
            }
            isset($data[1]) ? $verifyCode = strtolower($data[1]) : $verifyCode = '';
            $result = 11 - ($store % 11);
            if ($result == 10)
                $result = 'k';
            if ($result == 11)
                $result = 0;
            if ($verifyCode != $result) {
                $res = false;
            }
        }
        return $res;
    }

    // *********************** LOG ****************************

    public function saveLog($usuario, $request, $response) {
        $res = new \app\models\Log;
        $res->usuario_registrado = $usuario;
        $res->request = $request;
        $res->response = $response;
        $res->save();
        return true;
    }

    //************************ kilometros recorridos
    // https://rest.bermanngps.cl:40001/BermannRest/api/ulpos?tk=d48a79a37e3d65b45a57587bc0481902&patente=Camioneta%20Ford%20Arturo

    public function getToken() {
        $token = 0;
        $usuario = 'bermannsa';
        $clave = '3b24fa7e14555a1912e5bc2198f8e075';
        $url = $this->api_rest1 . 'login?user=' . $usuario . '&pass=' . $clave . '&app=1&os=1';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $resultArray = json_decode($result, true);
        if ($resultArray['estado'] == "OK") {
            if ($resultArray['mensaje'][0]['estado'] == "OK") {
                $token = $resultArray['mensaje'][0]['token'];
            }
        }
        return $token;
    }

    public function getKilometrosRecorridosPatente($patente) {
        $tk = $this->getToken();
        $url = $this->api_rest1 . 'ulpos?tk=' . $tk . '&patente=' . urlencode($patente);
        $res = array();
        $res[0] = '';
        $res[1] = '';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $result = curl_exec($ch);
        $request = $patente . '[' . $url . ']';
        $this->guardarLog($request, $result);
        curl_close($ch);
        $resultArray = json_decode($result, true);
        $valor = "n/a";
        $odom = "n/a";
        if ($resultArray['estado'] == "OK") {
            $mensaje = $resultArray['mensaje'];
            if (isset($mensaje[0]) == true && is_array($mensaje[0])) {
                // kmsdiarios
                $kmdiavo = isset($mensaje[0]['kmsdiavo']) == true ? $mensaje[0]['kmsdiavo'] : 0;
                $kmdiagps = isset($mensaje[0]['kmsdiagps']) == true ? $mensaje[0]['kmsdiagps'] : 0;
                $kmdiacanbus = isset($mensaje[0]['kmdiacanbus']) ? $mensaje[0]['kmdiacanbus'] : 0;
                $valor = $valor == "" && $kmdiacanbus > 0 ? intval($kmdiacanbus / 1000) : "";
                $valor = $valor == "" && $kmdiavo > 0 ? intval($kmdiavo / 1000) : $valor;
                $valor = $valor == "" && $kmdiagps > 0 ? intval($kmdiagps / 1000) : $valor;
                $res[0] = $valor > 0 ? number_format($valor, 0, "", "") : 0;
                // odom
                $odometrocanbus = isset($mensaje[0]['odometrocanbus']) == true ? $mensaje[0]['odometrocanbus'] : 0;
                $odometrogps = isset($mensaje[0]['odometrogps']) == true ? $mensaje[0]['odometrogps'] : 0;
                $odometrovo = isset($mensaje[0]['odometrovo']) ? $mensaje[0]['odometrovo'] : 0;
                $odom = $odom == "" && $odometrocanbus > 0 ? intval($odometrocanbus / 1000) : "";
                $odom = $odom == "" && $odometrovo > 0 ? intval($odometrovo / 1000) : $odom;
                $odom = $odom == "" && $odometrogps > 0 ? intval($odometrogps / 1000) : $odom;
                $res[1] = $odom > 0 ? number_format($odom, 0, "", "") : 0;
            }
        }
        return $res;
    }

    // log api 
    public function guardarLog($request, $response) {
        $res = '';
        if ($request != "" && $response != "") {
            $newLog = new \app\models\LogApi;
            $newLog->request = $request;
            $newLog->response = $response;
            $newLog->save();
        }
        return $res;
    }

    // *******************************  DCT ************************** 
    
    public function listadoPatentesArray() {
        $dataArray = [];
        $session = Yii::$app->session;
        $GruposDeUsuario = \app\models\GruposDctUsuarios::find()->where(["id_usuario" => $session["IdUsuario"]])->all();
        $vehiculosID = [];
        if ($GruposDeUsuario != false) {
            foreach ($GruposDeUsuario as $key => $value) {
                $grupoDetalle = Yii::$app->runAction('dct/vehiculosporgrupo', ["id" => $value["id_grupo_dct"], "token" => $session['tokenDCT']]);
                if ($grupoDetalle != "" && is_array($grupoDetalle) == true && count($grupoDetalle) > 0) {
                    foreach ($grupoDetalle as $kv => $vv) {
                        $vehiculosID[] = $vv;
                    }
                }
            }
        }
        $ultpos = [];
        if (count($vehiculosID) > 0) {
            $ultpos = Yii::$app->runAction('dct/ultposvehiculos', ["vehiculos" => implode(",", $vehiculosID), "token" => $session['tokenDCT']]);
        }
        $data = $ultpos;
        if (isset($data) == true && is_array($data) == true && count($data) > 0) {
            foreach ($data as $patente) {
                $patenteName = isset($patente->info) == true ? $patente->info->license_plate : "";
                $muestra = isset($patente->name) ? $patente->name : "";
                $id = $patenteName;
                $dataArray[$id] = $muestra . ' ( ' . $patenteName . ' )';
            }
        }
        return $dataArray;
    }
    
    
    public function getListadoGrupos() {
        $data = [];
        $token = Yii::$app->session->get('dct_token');
        ini_set("curl.cainfo", null);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dct.bermanngps.cl/api/groups",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 200,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{\n  \"username\": \"apidct@bermanngps.cl\",\n  \"password\": \"363combersaapidct\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Type: application/json",
                "Host: dct.bermanngps.cl",
                "accept-encoding: gzip, deflate",
                "content-length: 75",
                "Authenticate: " . $token,
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            // return "cURL Error #:" . $err;
        } else {
            $gruposJson = json_decode($response);
            if (isset($gruposJson->data) == true) {
                $grupos = $gruposJson->data;
                if (is_array($grupos) == true && count($grupos) > 0) {
                    foreach ($grupos as $grupo) {
                        $data[$grupo->id] = $grupo->name;
                    }
                }
                asort($data);
            }
        }
        return $data;
    }

    public function getPatentesArray() {
        $dataArray = [];
        Yii::$app->session->set('dct_patentes_array', $dataArray);
        $session = Yii::$app->session;
        $GruposDeUsuario = \app\models\GruposDctUsuarios::find()->where(["id_usuario" => $session["IdUsuario"]])->all();
        $vehiculosID = [];
        if ($GruposDeUsuario != false) {
            foreach ($GruposDeUsuario as $key => $value) {
                $grupoDetalle = Yii::$app->runAction('dct/vehiculosporgrupo', ["id" => $value["id_grupo_dct"], "token" => $session['tokenDCT']]);
                if ($grupoDetalle != "" && is_array($grupoDetalle) == true && count($grupoDetalle) > 0) {
                    foreach ($grupoDetalle as $kv => $vv) {
                        $vehiculosID[] = $vv;
                    }
                }
            }
        }
        $ultpos = [];
        if (count($vehiculosID) > 0) {
            $ultpos = Yii::$app->runAction('dct/ultposvehiculos', ["vehiculos" => implode(",", $vehiculosID), "token" => $session['tokenDCT']]);
        }
        $data = $ultpos;
        if (isset($data) == true && is_array($data) == true && count($data) > 0) {
            foreach ($data as $patente) {
                $patenteName = isset($patente->info) == true ? $patente->info->license_plate : "";
                $muestra = isset($patente->name) ? $patente->name : "";
                $id = $patente->id;
                $dataArray[$id] = $muestra . ' ( ' . $patenteName . ' )';
            }
        }
        Yii::$app->session->set('dct_patentes_array', $dataArray);
        return $data;
    }

    public function getPatentesArrayLista() {
        $data = Yii::$app->session->get('dct_patentes_array');
        $res = [];
        if (isset($data) == true && is_array($data) && count($data) > 0) {
            $res = $data;
        }
        return $res;
    }

    public function getPatenteName($code) {
        $res = '';
        $dataArray = Yii::$app->session->get('dct_patentes_array');
        if (isset($dataArray) == true && is_array($dataArray) == true && count($dataArray) > 0) {
            $res = isset($dataArray[$code]) == true ? $dataArray[$code] : '';
        }
        return $res;
    }

    public function getEventosArrayLista() {
        $data = Yii::$app->session->get('dct_eventos_array');
        $res = [];
        if (isset($data) == true && is_array($data) && count($data) > 0) {
            $res = $data;
        }
        return $res;
    }

    public function getEventosArrayListaEventos() {
        $data = Yii::$app->session->get('dct_eventos_array');
        $res = [];
        if (isset($data) == true && is_array($data) && count($data) > 0) {
            $codigos = "22,23,53,58,3,4,10,83,86,44,45";
            $myCodes = explode(',', $codigos);
            $newArray = array();
            foreach ($data as $key => $value) {
                if (in_array($key, $myCodes) == true) {
                    $newArray[$key] = $value;
                }
            }
            $res = $newArray;
        }
        return $res;
    }

    public function getEventosArray() {
        Yii::$app->session->set('dct_eventos_array', []);
        $db = Yii::$app->db;
        $sql = $db->createCommand("SELECT tts_v1 as id, teh.descripcion from ta_teh_standard_tts tts 
            inner join tipo_evento_homo_teh teh on teh.id = tts.teh_id 
            WHERE tts.tav_id in (1,3)");
        $eventos = $sql->queryAll();
        $eventoArray = [];
        if ($eventos != false) {
            foreach ($eventos as $evento) {
                $eventoArray[$evento["id"]] = $evento["descripcion"];
            }
            Yii::$app->session->set('dct_eventos_array', $eventoArray);
        }
        return $eventoArray;
    }

    public function getEventoName($code) {
        $res = '';
        $eventoArray = Yii::$app->session->get('dct_eventos_array');
        if (isset($eventoArray) == true && is_array($eventoArray) == true && count($eventoArray) > 0) {
            $res = isset($eventoArray[$code]) == true ? $eventoArray[$code] : '';
        }
        return $res;
    }

    public function getDatosFlota($vehiculos) {
        $res = [];
        ini_set("curl.cainfo", null);
        $curl = curl_init();
        $token = $token = Yii::$app->session->get('dct_token');
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dct.bermanngps.cl/api/vehicles?ids=$vehiculos&select=info,name,device:latest",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{\n  \"username\": \"apidct@bermanngps.cl\",\n  \"password\": \"363combersaapidct\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Type: application/json",
                "Host: dct.bermanngps.cl",
                "accept-encoding: gzip, deflate",
                "content-length: 75",
                "Authenticate: " . $token,
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $vehiculos = json_decode($response, true);
            return $vehiculos;
        }
    }

    public function getDatosHistoricos($vehiculos, $codes, $fechaDesde, $fechahasta) {
        $res = [];
        ini_set("curl.cainfo", null);
        $curl = curl_init();
        $token = $token = Yii::$app->session->get('dct_token');
        $finalUrl = "https://dct.bermanngps.cl/api/rawdata?vehicles=$vehiculos&fields=event_time,code,lat,lon,speed&order=-event_time";
        if ($codes != "") {
            $finalUrl = "https://dct.bermanngps.cl/api/rawdata?vehicles=$vehiculos&codes=$codes&fields=event_time,code,lat,lon,speed&order=-event_time";
        }
        if ($fechaDesde != "" && $fechahasta != "") {
            $finalUrl = $finalUrl . "&from=$fechaDesde&to=$fechahasta";
        }
        curl_setopt_array($curl, array(
            CURLOPT_URL => $finalUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Authenticate: " . $token,
                "Content-Type: application/json",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: dct.bermanngps.cl",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $res = "Error #:" . $err;
        } else {
            $historico = json_decode($response);
            if (isset($historico->events) == true) {
                $res = $historico->events;
            }
        }
        return $res;
    }

    public function getDatosEventos($vehiculos, $codes, $fechaDesde, $fechahasta) {
        $res = [];
        ini_set("curl.cainfo", null);
        $curl = curl_init();
        $token = $token = Yii::$app->session->get('dct_token');
        $finalUrl = "https://dct.bermanngps.cl/api/rawdata?vehicles=$vehiculos&fields=event_time,code,lat,lon,speed&order=vid,-event_time";
        if ($codes != "") {
            $finalUrl = "https://dct.bermanngps.cl/api/rawdata?vehicles=$vehiculos&codes=$codes&fields=event_time,code,lat,lon,speed&order=vid,-event_time";
        }
        if ($fechaDesde != "" && $fechahasta != "") {
            $finalUrl = $finalUrl . "&from=$fechaDesde&to=$fechahasta";
        }
        curl_setopt_array($curl, array(
            CURLOPT_URL => $finalUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Authenticate: " . $token,
                "Content-Type: application/json",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: dct.bermanngps.cl",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $res = "Error #:" . $err;
        } else {
            $historico = json_decode($response);
            if (isset($historico->events) == true) {
                $res = $historico->events;
            }
        }
        /* var_dump($response);
          die(); */
        return $res;
    }

    public function getVehiculosPorgrupoCSV($grupo) {
        ini_set("curl.cainfo", null);
        $curl = curl_init();
        $token = Yii::$app->session->get('dct_token');
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dct.bermanngps.cl/api/groups/$grupo",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{\n  \"username\": \"apidct@bermanngps.cl\",\n  \"password\": \"363combersaapidct\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Type: application/json",
                "Host: dct.bermanngps.cl",
                "accept-encoding: gzip, deflate",
                "content-length: 75",
                "Authenticate: " . $token,
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "";
        } else {
            $vehiculos = json_decode($response);
            $vehiculosArray = array();
            $data = isset($vehiculos->vehicles) == true ? $vehiculos->vehicles : [];
            if (count($data) > 0) {
                foreach ($data as $kv => $vv) {
                    $vehiculosArray[] = $vv;
                }
                return implode(',', $vehiculosArray);
            }
        }
    }

    public function getPatentesDataArrayCSV() {
        $dataArray = [];
        Yii::$app->session->set('dct_patentes_array', $dataArray);
        $session = Yii::$app->session;
        $GruposDeUsuario = \app\models\GruposDctUsuarios::find()->where(["id_usuario" => $session["IdUsuario"]])->all();
        $vehiculosID = [];
        if ($GruposDeUsuario != false) {
            foreach ($GruposDeUsuario as $key => $value) {
                $grupoDetalle = Yii::$app->runAction('dct/vehiculosporgrupo', ["id" => $value["id_grupo_dct"], "token" => $session['tokenDCT']]);
                if ($grupoDetalle != "" && is_array($grupoDetalle) == true && count($grupoDetalle) > 0) {
                    foreach ($grupoDetalle as $kv => $vv) {
                        $vehiculosID[] = $vv;
                    }
                }
            }
        }
        $data = implode(',', $vehiculosID);
        return $data;
    }

    public function getDatosIO($imei) {
        $res = [];
        ini_set("curl.cainfo", null);
        $curl = curl_init();
        $token = $token = Yii::$app->session->get('dct_token');
        $finalUrl = "https://dct.bermanngps.cl/api/devices/" . $imei . "?select=ios_state";
        curl_setopt_array($curl, array(
            CURLOPT_URL => $finalUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Authenticate: " . $token,
                "Content-Type: application/json",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: dct.bermanngps.cl",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $res = '';
        if ($err) {
            $res = "Error #:" . $err;
        } else {
            $res = json_decode($response, true);
        }
        return $res;
    }
    

}
