<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "grupos_dct_usuarios".
 *
 * @property string $id
 * @property string $id_grupo_dct
 * @property string $id_usuario
 *
 * @property Usuarios $usuario
 */
class GruposDctUsuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grupos_dct_usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_grupo_dct', 'id_usuario'], 'required'],
            [['id_grupo_dct', 'id_usuario'], 'default', 'value' => null],
            // [['id_grupo_dct'], 'integer'],
            // [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_grupo_dct' => 'Grupo Dct',
            'id_usuario' => 'Id Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'id_usuario']);
    }
}
