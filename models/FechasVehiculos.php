<?php

namespace app\models;

use Yii;
use yii\base\Model;

class FechasVehiculos extends Model
{
    public $fechaInicio;
    public $fechaFin;
    public $zonas;
    public $vehiculos;

   
    public function rules()
    {
        return [
            [['fechaInicio', 'fechaFin', 'vehiculos'], 'required', 'message' => 'Campo Requerido'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'fechaInicio' => 'Fecha Inicio',
            'fechaFin' => 'Fecha Fín',
            'vehiculos' => 'Vehiculos',
        ];
    }

    public function unsetAttributes($names=null)
    {
        if($Nombre===null)
            $Nombre=$this->attributeNames();
        foreach($Nombre as $name)
            $this->$name=null;
    }

}