<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

    $this->title = 'Listado de usuarios por grupos';
    $bread1  = 'Grupos-Usuarios';
    $bread2  = $this->title;
    $bread3  = '';
    $this->params['activeLink'] = "grupousuario";
?>

<!-- DataTables -->
<link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<!-- Responsive datatable examples -->
<link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<!-- scripts -->
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Buttons examples -->
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/jszip.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/pdfmake.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/vfs_fonts.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/buttons.html5.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/buttons.print.min.js"></script>


<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h2 class="page-title" style="font-size: 16px;"><?= $this->title ?></h2>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><?= $bread1 ?></li>
                <?php if ($bread2  != '') { ?>
                    <li class="breadcrumb-item">
                        <?= $bread2 ?>
                    </li>
                <?php } ?>
                <?php if ($bread3  != '') { ?>
                    <li class="breadcrumb-item"><?= $bread3 ?></li>
                <?php } ?>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>


<div class="row">

    <div class="col-md-12">
        <div class="card-box table-responsive">
            <a href="<?= Yii::getAlias('@web'); ?>/gruposusuarios/crear"  class="btn btn-success waves-effect waves-light">Asociar usuario a grupo dct</a>
            <a href="<?= Yii::getAlias('@web'); ?>/usuarios/crear"  class="btn btn-success waves-effect waves-light">Crear usuario</a>

            <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>id usuario</th>
                        <th>Usuario</th>
                        <th width="50">cantidad de grupos</th>
                        <th width="20">Editar</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($gruposusarios as $key => $value) { ?>
                        <tr>
                            <td><?= $value["id_usuario"] ?></td>
                            <td><?= $value["usuario"] ?></td>
                            <td><?= $value["cantidad_grupos"] ?></td>

                            <td class="text-center">

                                <a href="<?= Yii::getAlias('@web'); ?>/gruposusuarios/editar/<?= $value["id_usuario"]?>" class="btn btn-sm btn-primary"><i class="fal fa-edit" ></i></a>

                            </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>

        </div>  
    </div>
</div>


<div class="modal fade" id="modalSensor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar nombre</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'method' => 'post', 
                    'id'=> 'CrearUsuario', 
                    'action' => Url::to(['site/guardarnombresensor']),
                    'options'=> [
                        'class' => 'form-horizontal',
                    ], 
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => true, 
                    ]); ?>
                        
                        <div class="row">

                            <div  class="form-group col-md-12">
                                <label for="st_name">Nombre del sensor</label>
                                <input class="form-control" type="text" name="st_name" id="st_name" required >
                                <input class="form-control" type="hidden" name="st_mac" id="st_mac" >
                            </div>

                        </div>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <?= Html::submitButton(Yii::t('app', '<i class="fa fa-check"></i> Guardar'), ['class' => 'btn btn-success waves-effect waves-light', 'id'=>'btn_guardar']) ?>
            </div>
            <?php $form->end(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">

$(document).ready(function () {
    $('#responsive-datatable').DataTable({
        dom: 'Bfrtip',
        buttons: [  ],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
            "sEmptyTable": "Sin datos para mostrar"
        },
        "columns": [
            { "width": "5%" },
            null,
            { "width": "10%" },
            { "width": "5%" },
        ],
    });
    
});

$("#patente_div, #header_listado").remove();

</script>

