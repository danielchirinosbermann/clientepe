<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Agregar usuarios a grupo de DCT';
$bread1 = 'Grupos-Usuarios';
$bread2 = 'Lista de grupos-usuarios';
$bread3 = $this->title;
$this->params['activeLink'] = "grupousuario";
?>

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h2 class="page-title" style="font-size: 16px;"><?= $this->title ?></h2>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><?= $bread1 ?></li>
                <?php if ($bread2 != '') { ?>
                    <li class="breadcrumb-item">
                        <a href="<?= Yii::getAlias('@web'); ?>/gruposusuarios/lista"><?= $bread2 ?></a>
                    </li>
                <?php } ?>
                <?php if ($bread3 != '') { ?>
                    <li class="breadcrumb-item"><?= $bread3 ?></li>
                <?php } ?>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">

            <?php
            $form = ActiveForm::begin([
                        'method' => 'post',
                        'id' => 'GrupoUsuarioForm',
                        'options' => [
                            'class' => 'form-horizontal',
                        ],
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true,
            ]);
            ?>
            
            <div style="padding-left:50px;">
                <div class="row">
                    <div id="" class="form-group col-md-3">
                        <?= $form->field($model, 'id_usuario')->dropdownList($usuarios, ['onchange'=>'getGruposPorUsuario(this.value);', 'class' => 'selectpicker form-control', 'data-live-search' => 'true', 'data-actions-box' => "true", 'prompt' => 'Seleccione', 'title' => "Usuarios"])->error(['style'=>'color:white', 'class' => 'help-block badge badge-danger']) ?>
                    </div>      
                </div>
                <div class="row">
                    <div id="drop_grupos_dct" class="form-group col-md-3" style="">
                        <?= $form->field($model, 'id_grupo_dct')->dropdownList($grupos, ['size'=>10, 'class' => 'selectpicker form-control', 'data-live-search' => 'true', 'title' => "Grupos DCT", "multiple" => "multiple"])->error(['style'=>'color:white', 'class' => 'help-block badge badge-danger']) ?>
                    </div>                          
                </div>


                <div class="row" style="padding-left:10px">
                    <div class="form-actions">
                        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-check"></i> Guardar'), ['class' => 'btn btn-success waves-effect waves-light', 'id' => 'btn_guardar']) ?>
                    </div>
                </div>
            </div>

<?php $form->end(); ?>

        </div>
    </div>
</div>


<script type="text/javascript">
    
    function getGruposPorUsuario(id){
        // limpio los valores seleccionados antes
        $("#gruposdctusuarios-id_grupo_dct").val([]);
        if (id > 0){
            $.ajax({
                    type: "POST",
                    url: "<?php echo Yii::$app->request->baseUrl . '/gruposusuarios/getgruposporusuario'; ?>",
                    dataType: "html",
                    data: {id: id},
                    success: function (response) {
                        // console.log(response);
                        $("#gruposdctusuarios-id_grupo_dct").val([response]);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Problemas enviado datos : \n" + xhr.responseText);
                    }
                });
        }
    }
    
    $("#patente_div, #header_listado").remove();

</script>


