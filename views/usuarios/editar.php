<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Editar usuario';
$this->params['bread1'] = $this->title;
$this->params['bread2'] = '';
$this->params['bread3'] = '';
$this->params['activeLink'] = "usario-listado";
?>
<legend>Editar Usuario - <?= $model->usuario; ?></legend>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <?php
            $form = ActiveForm::begin([
                        'method' => 'post',
                        'id' => 'EditarUsuario',
                        'options' => [
                            'class' => 'form-horizontal',
                        ],
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true,
            ]);
            ?>
            <div style="padding-left:50px;">
                <div class="row">
                    <div  class="form-group col-md-3">
                        <?= $form->field($model, 'usuario', ['template' => '{label}{input}{error}{hint}', 'errorOptions' => ['class' => 'badge badge-danger']
                        ])->textInput(['class' => 'form-control', 'placeholder' => 'Usuario acceso al sistema'])
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div  class="form-group col-md-3">
                        <?= $form->field($model, 'clave', ['template' => '{label}{input}{error}{hint}', 'errorOptions' => ['class' => 'badge badge-danger']
                        ])->textInput(['class' => 'form-control', 'placeholder' => 'Clave', 'type' => 'text'])
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-actions">
                    <?= Html::submitButton(Yii::t('app', '<i class="fa fa-check"></i> Guardar'), ['class' => 'btn btn-success waves-effect waves-light', 'id' => 'btn_guardar']) ?>
                </div>
            </div>

<?php $form->end(); ?>

        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $("#patente_div, #header_listado").remove();
    });


</script>

