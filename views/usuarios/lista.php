<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

    $this->title = 'Listado de usuarios';
    $bread1  = 'Usuarios';
    $bread2  = $this->title;;
    $bread3  = '';
    $this->params['activeLink'] = "usario-listado";
?>

<!-- DataTables -->
<link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<!-- Responsive datatable examples -->
<link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<!-- scripts -->
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Buttons examples -->
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/jszip.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/pdfmake.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/vfs_fonts.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/buttons.html5.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/buttons.print.min.js"></script>


<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h2 class="page-title" style="font-size: 16px;"><?= $this->title ?></h2>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><?= $bread1 ?></li>
                <?php if ($bread2  != '') { ?>
                    <li class="breadcrumb-item">
                        <?= $bread2 ?>
                    </li>
                <?php } ?>
                <?php if ($bread3  != '') { ?>
                    <li class="breadcrumb-item"><?= $bread3 ?></li>
                <?php } ?>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card-box table-responsive">
            <a href="<?= Yii::getAlias('@web') ?>/usuarios/crear" class="btn btn-success waves-effect waves-light">Crear usuario</a>
            <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Usuario</th>
                        <th>Editar</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($usuarios as $key => $value) { ?>
                        <tr>
                            <td><?= $value->id ?></td>
                            <td><?= $value->usuario ?></td>
                            <td class="text-center">

                          
                                <a href="<?= Yii::getAlias('@web'); ?>/usuarios/editar/<?= $value->id ?>" class="btn btn-sm btn-primary">
                                    <i class="fal fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>

        </div>  
    </div>
</div>

<script>

$(document).ready(function () {
    $('#responsive-datatable').DataTable({
        dom: 'Bfrtip',
        buttons: [  ],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
            "sEmptyTable": "Sin datos para mostrar"
        },
        "columns": [
            { "width": "5%" },
            null,
            { "width": "5%" },
        ],
    });

});

$("#patente_div, #header_listado").remove();

</script>

