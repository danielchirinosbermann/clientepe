<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

    $this->title = 'Eventos Físicos';
    $this->params['bread1']  = $this->title;
    $this->params['bread2']  = '';
    $this->params['bread3']  = '';
    $this->params['activeLink'] = "sesnores-listado";
?>

<!-- DataTables -->
<link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<!-- Responsive datatable examples -->
<link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<!-- scripts -->
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Buttons examples -->
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/jszip.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/pdfmake.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/vfs_fonts.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/buttons.html5.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/datatables/buttons.print.min.js"></script>


<div class="row">
    <div class="col-md-12">
        <div class="card-box table-responsive">
            <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Mac</th>
                        <th>Serial</th>
                        <th>Nombre</th>
                        <th>Modelo</th>
                        <th>Editar</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($sensores as $key => $value) { ?>
                        <tr>
                            <td><?= $value["st_mac"] ?></td>
                            <td><?= $value["st_serial_number"] ?></td>
                            <td><?= $value["st_name"] ?></td>
                            <td><?= $value["st_model"] ?></td>
                            <td class="text-center">

                          
                                <button class="btn btn-sm btn-primary" onclick="cambiarNombre('<?= $value['st_mac'] ?>')">
                                    <i class="fal fa-edit"></i>
                                </button>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>

        </div>  
    </div>
</div>


<div class="modal fade" id="modalSensor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar nombre</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'method' => 'post', 
                    'id'=> 'CrearUsuario', 
                    'action' => Url::to(['site/guardarnombresensor']),
                    'options'=> [
                        'class' => 'form-horizontal',
                    ], 
                    
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => true, 
                    ]); ?>
                        
                        <div class="row">

                            <div  class="form-group col-md-12">
                                <label for="st_name">Nombre del sensor</label>
                                <input class="form-control" type="text" name="st_name" id="st_name" required >
                                <input class="form-control" type="hidden" name="st_mac" id="st_mac" >
                            </div>

                        </div>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <?= Html::submitButton(Yii::t('app', '<i class="fa fa-check"></i> Guardar'), ['class' => 'btn btn-success waves-effect waves-light', 'id'=>'btn_guardar']) ?>
            </div>
            <?php $form->end(); ?>
        </div>
    </div>
</div>

<script>

$(document).ready(function () {
    $('#responsive-datatable').DataTable({
        dom: 'Bfrtip',
        buttons: [  ],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
            "sEmptyTable": "Sin datos para mostrar"
        },
    });

});



</script>
