<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyCeWLXP10h_pemnBj0gYDp_9-wICMqeR9I&libraries=geometry,places"></script>
<?php

// AIzaSyCeWLXP10h_pemnBj0gYDp_9-wICMqeR9I

use yii\helpers\Html;

$this->title = 'Inicio';
$this->params['bread1'] = $this->title;
$this->params['bread2'] = '';
$this->params['bread3'] = '';
$this->params['activeLink'] = "inicio";

ini_set('max_execution_time', '99600');
ini_set('max_input_time', '-1');
ini_set('memory_limit', '-1');
set_time_limit(9600);

$imagenFlechaVerde = '<img src="' . Yii::getAlias('@web') . '/images/flecha_verde.png" style="width:16px;height:16px;" />';
$imagenFlechaAmarilla = '<img src="' . Yii::getAlias('@web') . '/images/flecha_amarilla.png" style="width:16px;height:16px;" />';
$imagenFlechaRoja = '<img src="' . Yii::getAlias('@web') . '/images/flecha_roja.png" style="width:16px;height:16px;" />';
$imagenFlechaStop = '<img src="' . Yii::getAlias('@web') . '/images/stop_1.png" style="width:16px;height:16px;" />';
$imagenFlechaStop2 = '<img src="' . Yii::getAlias('@web') . '/images/stop_2.png" style="width:16px;height:16px;" title="Vehiculo sin información" />';
$imagenFlechaStop3 = '<img src="' . Yii::getAlias('@web') . '/images/stop_3.png" style="width:16px;height:16px;" title="Vehiculo sin información" />';
?>

<style>
    #map{
        height: calc(100vh - 132px);
    }

    .content{
        margin-bottom: unset !important;
        padding: unset !important;
    }
    .container-fluid {
        padding-right: unset;
        padding-left: unset;

    }

    .card-box {
        padding: unset;
        border: none;
        border-radius: unset;
        -moz-border-radius: unset;
        margin-bottom: unset;
    }
</style>

<div class="row">
    <div class="col-12">
        <div id="mensajes" style="padding:5px;text-align:center;display:none"></div>
        <table border="1" cellspacing="2" cellpadding="2" style="width:100%">
            <tbody>
                <tr>
                    <td id="comando_td" style="vertical-align:top;width:200px;display:none">
                        <div id="comandos" style="">&nbsp;</div>  
                    </td>
                    <td style="vertical-align:top">
                        <div id="map" class="gmaps"></div>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>
</div>

<script>

    var vehiculos = [];
    var mostrar = 1;
    var actualizo = 0;
    var pendientes = [];
    var procesados = [];
    var procesaPendientes = 0;

    var flecha_verde = '<?= $imagenFlechaVerde; ?>';
    var flecha_amarilla = '<?= $imagenFlechaAmarilla; ?>';
    var flecha_roja = '<?= $imagenFlechaRoja; ?>';
    var flecha_stop = '<?= $imagenFlechaStop; ?>';
    var flecha_stop2 = '<?= $imagenFlechaStop2; ?>';
    var flecha_stop3 = '<?= $imagenFlechaStop3; ?>';


    $(document).ready(function () {
        initialize();
        setInterval(function () {
            if (actualizo == 1) {
                loadDataRefresh();
            }
        },30000);
        
        setInterval(function () {
            if (procesaPendientes == 1) {
                // console.log("procesados = "+procesados.length + " ---- pendientes = "+cuentaPendientes());
                procesaCidPendientes();
            }
        },5000);
        
        
    });
    
    function getTiempoDetenido(v){
       var res = "";
       if (v.device.latest.data.io_ign != undefined) {
           if (v.device.latest.data.io_ign.value == false){
               var datoprevio = v.device.latest.data.io_ign.change.evtime;
               var datoactual = v.device.latest.data.io_ign.evtime;
               var fechaPrevia = new Date(datoprevio*1000);
               var fechaActual = new Date(datoactual*1000);
               var diffMs = (fechaActual - fechaPrevia);
               var seg = diffMs / 1000;
               var diffMins = parseInt(seg / 60);
               res = diffMins;
           } else {
               res = 0;
           }
       }
       return res;
    }
    
    function getTiempoDetenidoLabel(v){
       var res = "";
       if (v.device.latest.data.io_ign != undefined) {
           if (v.device.latest.data.io_ign.value == false){
               var datoprevio = v.device.latest.data.io_ign.change.evtime;
               var datoactual = v.device.latest.data.io_ign.evtime;
               var fechaPrevia = new Date(datoprevio*1000);
               var fechaActual = new Date(datoactual*1000);
               var diffMs = (fechaActual - fechaPrevia);
               var seg = diffMs / 1000;
               res = convertSeconds(seg);
           } else {
               res = 0;
           }
       }
       return res;
    }
    
    
    var map = null;
    var markersArray = [];
    var vehiculosArray = [];
    var geocoder = null;
    var direccion_patente = [];
    var infowindowArray = [];

    function getIconoMovil(v) {
        // logica iconos y colores 
        var color = 0;
        var rotacion = v.device.latest.loc.head;
        var velocidad = parseInt(v.device.latest.loc.mph * 1.6);
        if (v.device.latest.vcounters.vehicle_dev_idle != undefined) {
            var tiempo_detenido_minutos = getTiempoDetenido(v);
            // var segundos = v.device.latest.vcounters.vehicle_dev_idle;
            // var tiempo_detenido_minutos = (segundos / 60);
            // verde
            if (tiempo_detenido_minutos > 0 && tiempo_detenido_minutos <= 5 && velocidad < 5) {
                color = 1;
            }
            // roja
            if (tiempo_detenido_minutos > 5 && tiempo_detenido_minutos <= 25 && velocidad < 5) {
                color = 2;
            }
            // stop rojo
            if (tiempo_detenido_minutos > 25 && tiempo_detenido_minutos <= 2900 && velocidad < 5) {
                color = 4;
            }
            // stop negro 
            if (tiempo_detenido_minutos > 2900 && velocidad < 5) {
                color = 5;
            }
        } else {
            color = 0;
        }
        var fillColor = ["green", "yellow", "red", "white"];
        var shape = {
            gray: 'm 15.832471,-142.36175 c 0,0 121.076359,-2.15352 114.751919,317.15236 -83.675559,-59.30589 -133.675559,-59.30589 -229.503849,0 -4.171711,-319.30588 115.82829,-319.30588 114.75193,-317.15236 z',
            blue: 'm -35.860415,-201.21297 c 0,0 76.278938,198.1153079 110.756059,306.10857 -95.39889,-79.999013 -125.957928,-58.687449 -221.512124,0 z'
        };
        var url = "<?= Yii::getAlias('@web'); ?>/images/stop_1.png";
        var url2 = "<?= Yii::getAlias('@web'); ?>/images/stop_3.png";

        // stop
        if (color == 4) {
            var image = {
                url: url,
                size: new google.maps.Size(32, 32),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 32)
            };
        }
        if (color == 5) {
            var image = {
                url: url2,
                size: new google.maps.Size(32, 32),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 32)
            };
        }

        if (color < 4) {
            var image = {
                path: shape["blue"], fillColor: fillColor[color],
                fillOpacity: 1,
                scale: 0.1, strokeColor: "black",
                strokeWeight: 1,
                rotation: parseInt(rotacion)};
        }
        return image;
    }

    function initialize() {
        var mapOptions = {
            center: new google.maps.LatLng(-33.446420, -70.660569),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: 12
        };
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        geocoder = new google.maps.Geocoder();
        loadData();
    }

    function convertDate(epoch) {
        var fecha = new Date(epoch * 1000);
        var mes = fecha.getMonth() + 1;
        var minutos = fecha.getMinutes() < 10 ? '0' + fecha.getMinutes() : fecha.getMinutes();
        var segundos = fecha.getSeconds() < 10 ? '0' + fecha.getSeconds() : fecha.getSeconds();
        return fecha.getDate() + "/" + mes + "/" + fecha.getFullYear() + " " + fecha.getHours() + ":" + minutos + ":" + segundos;
    }
    
    function convertDateListado(epoch) {
        var fecha = new Date(epoch * 1000);
        var mes = fecha.getMonth() + 1;
        var minutos = fecha.getMinutes() < 10 ? '0' + fecha.getMinutes() : fecha.getMinutes();
        var segundos = fecha.getSeconds() < 10 ? '0' + fecha.getSeconds() : fecha.getSeconds();
        var dia = fecha.getDate() < 10 ? '0'+fecha.getDate() : fecha.getDate();
        return  dia + "/" + mes + "  " + fecha.getHours() + ":" + minutos;
    }

    function convertSeconds(seconds) {
        var days = Math.floor(seconds / (3600*24));
        seconds  -= days*3600*24;
        var hrs   = Math.floor(seconds / 3600);
        seconds  -= hrs*3600;
        var mnts = Math.floor(seconds / 60);
        seconds  -= mnts*60;
        return days > 0 ? (days+" Dias "+hrs+"hrs:"+ mnts + "min:" + seconds+"seg") : hrs+"hrs:"+ mnts + "min:" + seconds+"seg"; 
    }

    function loadMarkets() {
        markersArray = [];
        $("#cantidad_vehiculos").html(vehiculos.length);
        if (vehiculos.length > 0) {
            // ahora los marcadores
            $.each(vehiculos, function (i, v) {
                vehiculosArray[v.id] = v;
                var iconoMovil = getIconoMovil(v);
                var marker = new google.maps.Marker({
                    position: {lat: parseFloat(v.device.latest.loc.lat), lng: parseFloat(v.device.latest.loc.lon)},
                    map: map,
                    icon: iconoMovil
                });
                marker.vehiculo = v.id;
                markersArray[v.id] = marker;
                var infowindow = new google.maps.InfoWindow;
                marker.addListener('click', function (event) {
                    if (infowindow) {
                        infowindow.close();
                    }
                    getDireccion(v, infowindow, marker, event);
                });
                google.maps.event.addListener(map, "click", function (event) {
                    infowindow.close();
                });
            });
            // console.log(vehiculosArray);
        }
    }

    function actualizaFlechasListado() {
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/get-ulpos'; ?>",
            dataType: "html",
            async: true,
            success: function (response) {
                vehiculos = JSON.parse(response);
                if (vehiculos.length > 0) {
                    $.each(vehiculos, function (i, v) {
                        var flecha = flecha_verde;
                        var vehiculo = v.id;
                        var rotacion = v.device.latest.loc.head;
                        var velocidad = parseInt(v.device.latest.loc.mph * 1.6);
                        var fecha = v.device.latest.loc.evtime;
                        var fechanueva = '';
                        var detenido = 0;
                        if (v.device.latest.loc.evtime!=undefined){
                                    var tiempo = v.device.latest.loc.evtime;
                                    if (tiempo != "") {
                                       fechanueva = convertDateListado(tiempo);
                                    }
                        }
                        
                        var tiempo_detenido_minutos = getTiempoDetenido(v);
                        
                        // verde
                        if (tiempo_detenido_minutos > 0 && tiempo_detenido_minutos <= 5 && velocidad < 5) {
                            flecha = flecha_amarilla;
                        }
                        // roja
                        if (tiempo_detenido_minutos > 5 && tiempo_detenido_minutos <= 25 && velocidad < 5) {
                            flecha = flecha_roja;
                        }
                        // stop rojo
                        if (tiempo_detenido_minutos > 25 && tiempo_detenido_minutos <= 2900 && velocidad < 5) {
                            flecha = flecha_stop;
                        }
                        // stop negro 
                        if (tiempo_detenido_minutos > 2900 && velocidad < 5) {
                            flecha = flecha_stop3;
                        }
                        
                        /*if (v.device.latest.vcounters.vehicle_dev_idle != undefined) {
                            var segundos = v.device.latest.vcounters.vehicle_dev_idle;
                            var tiempo_detenido_minutos = (segundos / 60);
                            detenido = tiempo_detenido_minutos;
                            // verde
                            if (tiempo_detenido_minutos > 0 && tiempo_detenido_minutos <= 5 && velocidad < 5) {
                                flecha = flecha_amarilla;
                            }
                            // roja
                            if (tiempo_detenido_minutos > 5 && tiempo_detenido_minutos <= 25 && velocidad < 5) {
                                flecha = flecha_roja;
                            }
                            // stop rojo
                            if (tiempo_detenido_minutos > 25 && tiempo_detenido_minutos <= 2880 && velocidad < 5) {
                                flecha = flecha_stop;
                            }
                            // stop negro 
                            if (tiempo_detenido_minutos > 2900 && velocidad < 5) {
                                flecha = flecha_stop3;
                            }
                        } else {
                            flecha = flecha_verde;
                        }*/
                        
                        if (velocidad > 5){
                            flecha = flecha_verde;
                        }
                        if (fecha == undefined || fecha == '' || fecha == null){
                            flecha = flecha_stop2;
                        }
                        
                        $('.patente_icon').each(function(index){
                            var dataId = $(this).data('patente');
                            if (dataId == v.id){
                                $(this).html(flecha);
                            }
                        });
                        $('.patente_fecha').each(function(index){
                            var dataId = $(this).data('patente');
                            if (dataId == v.id){
                                $(this).html(fechanueva);
                            }
                        });
                        
                        
                    });
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                // 
            }
        });
    }

    function loadDataRefresh() {
        direccion_patente = [];
        // elimino todos los marcadores previos 
        var marcadoresOld = markersArray;
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/get-ulpos'; ?>",
            dataType: "html",
            async: true,
            success: function (response) {
                vehiculos = JSON.parse(response);
                loadMarkets();
                $.each(marcadoresOld, function (e) {
                    if (this != undefined && this.setMap != undefined) {
                        this.setMap(null);
                    }
                });
                actualizaFlechasListado();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#mensajes").html('');
                $("#mensajes").hide("slow");
            }
        });
        return true;
    }

    function loadData() {
        direccion_patente = [];
        $("#mensajes").html('<i style="color:red;vertical-align:middle" class="fas fa-cog fa-spin fa-2x"></i><span style="color:red;font-size:14px;margin-left:10px;margin-top:5px;">Buscando información de las posiciones de los vehículos, por favor espere...</span>');
        $("#mensajes").show("slow");
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/get-ulpos'; ?>",
            dataType: "html",
            async: true,
            /*data: {id: id},*/
            success: function (response) {
                vehiculos = JSON.parse(response);
                // console.log(vehiculos);
                $("#mensajes").html('');
                $("#mensajes").hide("slow");
                loadGrupos();
                loadMarkets();
                actualizaFlechasListado();
                actualizo = 1;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#mensajes").html('');
                $("#mensajes").hide("slow");
                // alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
        return true;
    }

    function setMostrar(valor) {
        if (mostrar == valor) {
            return false;
        }
        mostrar = valor;
        loadGrupos();
    }

    function loadGrupos() {
        $("#mensajes").html('<i style="color:red;vertical-align:middle" class="fas fa-cog fa-spin fa-2x"></i><span style="color:red;font-size:14px;margin-left:10px;margin-top:5px;">Buscando información de los grupos y vehículos...</span>');
        $("#mensajes").show("slow");
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/get-grupos'; ?>",
            dataType: "html",
            async: true,
            data: {mostrar: mostrar},
            success: function (response) {
                $("#mensajes").html('');
                $("#mensajes").hide("slow");
                $("#listados").html(response);
                $('#listado_grupos .panel-collapse').collapse('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#mensajes").html('');
                $("#mensajes").hide("slow");
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
        return true;

    }
    
    function getIOs(v){
        var io_pwr = 'circulo_gris';
        var io_pwr_title = 'estado POWER ON';
        
        var io_ign = 'circulo_gris';
        var io_ign_title = 'estado IGNICION';
        
        var io_out1 = 'circulo_gris';
        var io_out1_title = 'CORTA CORRIENTE';
        
        var io_out2 = 'circulo_gris';
        var io_out2_title = 'PARADA SEGURA';
        
        var io_in1 = 'circulo_gris';
        var io_in1_title = 'PUERTA COPILOTO';
        
        var io_in2 = 'circulo_gris';
        var io_in2_title = 'BOTON PANICO';
        
        var io_in3 = 'circulo_gris';
        var io_in3_title = 'PUERTA PILOTO';
        
        if (v.device.latest.ios.io_pwr!=undefined){
           io_pwr = v.device.latest.ios.io_pwr == true ? 'circulo_verde' : 'circulo_rojo';
           io_pwr_title = v.device.latest.ios.io_pwr == true ? 'POWER ON' : 'POWER OFF';
        }
        if (v.device.latest.ios.io_ign!=undefined){
           io_ign = v.device.latest.ios.io_ign == true ? 'circulo_verde' : 'circulo_rojo';
           io_ign_title = v.device.latest.ios.io_ign == true ? 'IGNICION ON' : 'IGNICION OFF';
        }
        if (v.device.latest.ios.io_out1!=undefined){
           io_out1 = v.device.latest.ios.io_out1 == true ? 'circulo_rojo' : 'circulo_verde';
           io_out1_title = v.device.latest.ios.io_out1 == true ? 'CORTA CORRIENTE ACTIVO' : 'CORTA CORRIENTE INACTIVO';
        }
        if (v.device.latest.ios.io_out2!=undefined){
           io_out2 = v.device.latest.ios.io_out2 == true ? 'circulo_rojo' : 'circulo_verde';
           io_out2_title = v.device.latest.ios.io_out2 == true ? 'PARADA SEGURA ACTIVA' : 'PARADA SEGURA INACTIVA';
        }
        
        if (v.device.latest.ios.io_in1!=undefined){
           io_in1 = v.device.latest.ios.io_in1 == true ? 'circulo_rojo' : 'circulo_verde';
           io_in1_title = v.device.latest.ios.io_in1 == true ? 'PUERTA COPILOTO ABIERTA' : 'PUERTA COPILOTO CERRADA';
        }
        
        if (v.device.latest.ios.io_in2!=undefined){
           io_in2 = v.device.latest.ios.io_in2 == true ? 'circulo_rojo' : 'circulo_verde';
           io_in2_title = v.device.latest.ios.io_in2 == true ? 'BOTON PANICO ACTIVO' : 'BOTON PANICO INACTIVO';
        }
        
        if (v.device.latest.ios.io_in3!=undefined){
           io_in3 = v.device.latest.ios.io_in3 == true ? 'circulo_rojo' : 'circulo_verde';
           io_in3_title = v.device.latest.ios.io_in3 == true ? 'PUERTA PILOTO ABIERTA' : 'PUERTA PILOTO CERRADA';
        }
        
        
        
        var res = '<table border="0" style="font-size:11px" class="table table-condensed"><thead><tr>' +
                '<th style="text-align:center">Power ON</th>'+
                '<th style="text-align:center">Ignicion ON</th>'+
                '<th style="text-align:center">Corta Corriente</th>'+
                '<th style="text-align:center">Parada Segura</th>'+
                '<th style="text-align:center">Puerta Copiloto</th>'+
                '<th style="text-align:center">Boton Panico</th>'+
                '<th style="text-align:center">Puerta Piloto</th>'+
                '</tr></thead><tbody>'+ 
                '<tr>' +
                    '<td style="text-align:center"><div title="'+io_pwr_title+'" class="'+io_pwr+'">&nbsp;</div></td>' +
                    '<td style="text-align:center"><div title="'+io_ign_title+'"  class="'+io_ign+'">&nbsp;</div></td>' +
                    '<td style="text-align:center"><div title="'+io_out1_title+'"  class="'+io_out1+'">&nbsp;</div></td>' +
                    '<td style="text-align:center"><div title="'+io_out2_title+'"  class="'+io_out2+'">&nbsp;</div></td>' +
                    '<td style="text-align:center"><div title="'+io_in1_title+'"  class="'+io_in1+'">&nbsp;</div></td>' +
                    '<td style="text-align:center"><div title="'+io_in2_title+'"  class="'+io_in2+'">&nbsp;</div></td>' +
                    '<td style="text-align:center"><div title="'+io_in3_title+'"  class="'+io_in3+'">&nbsp;</div></td>' +
            '</tr>' +
            '</tbody></table>'; 
        return res;
    }   
    

    function getDireccion(vehiculoData, infow, mark, event) {
        var v = vehiculoData;
        var lat = parseFloat(v.device.latest.loc.lat);
        var lng = parseFloat(v.device.latest.loc.lon);
        if (lat == undefined || lat == '') {
            return false;
        }
        if (lng == undefined || lng == '') {
            return false;
        }
        var busqueda = lat + " " + lng;
        geocoder.geocode({address: busqueda}, direccionMarket);
        function direccionMarket(arreglo, estado) {
            if (estado == "OK") {
                var ios = v.device.latest.ios;
                console.log(v);
                console.log(getTiempoDetenido(v));
                // console.log(ios);
                
                
                var direccion = arreglo[0].formatted_address;
                var muestra = v.name;
                var patente = '';
                var fecha = '';
                var lat = parseFloat(v.device.latest.loc.lat);
                var lon = parseFloat(v.device.latest.loc.lon);
                var velocidad = parseInt(v.device.latest.loc.mph * 1.6);
                var tiempo = '';
                if (v != undefined && v.device != undefined) {
                    patente = v.info.license_plate;
                    fecha = convertDate(v.device.latest.loc.evtime);
                }
                tiempo = getTiempoDetenidoLabel(v);
                
                var datos = document.createElement('div');
                datos.style.width = '500px';
                datos.style.height = '350px';
                var close = document.createElement('div');
                close.align = "center";
                var closeLink = document.createElement('a');
                closeLink.innerHTML = "<span class='btn btn-danger btn-mini'><b>Cerrar</b></span>";
                closeLink.href = "#";
                closeLink.onclick = function () {
                    infow.close();
                };
                var ios = getIOs(v);
                close.appendChild(closeLink);
                var contentString = document.createElement('div');
                contentString.align = "left";
                contentString.innerHTML = '<div style="padding:5px;width:auto;">' +
                        '<i class="fas fa-truck-moving fa-2x text-success"></i> <b><span style="margin-left:5px;font-size:1.5em">Detalle Vehículo</span></b>' +
                        '<br/><br/>' +
                        '<table border="0" cellspacing="2" cellpadding="2"><tbody>' +
                        '<tr><td style="text-align:left;padding:4px;width:120px;"><b>Fecha : </b></td><td>&nbsp;&nbsp;</td><td>' + fecha + '</td></tr>' +
                        '<tr><td style="text-align:left;padding:4px;"><b>Patente : </b></td><td>&nbsp;&nbsp;</td><td>' + patente + '</td></tr>' +
                        '<tr><td style="text-align:left;padding:4px;"><b>Muestra : </b></td><td>&nbsp;&nbsp;</td><td>' + muestra + '</td></tr>' +
                        '<tr><td style="text-align:left;padding:4px;"><b>Direccion : </b></td><td>&nbsp;&nbsp;</td><td>' + direccion + '</td></tr>' +
                        '<tr><td style="text-align:left;padding:4px;"><b>Velocidad : </b></td><td>&nbsp;&nbsp;</td><td>' + velocidad + ' </td></tr>' +
                        '<tr><td style="text-align:left;padding:4px;"><b>Tiempo Detenido : </b></td><td>&nbsp;&nbsp;</td><td>' + tiempo + '</td></tr>' +
                        '<tr><td style="text-align:left;padding:4px;"><b>Eventos : </b></td><td>&nbsp;&nbsp;</td><td>' + ios + '</td></tr>' +
                        '</tbody></table>' +
                        '</div>';
                datos.appendChild(contentString);
                datos.appendChild(close);
                infow.setContent(datos);
                if (event != undefined) {
                    infow.setPosition(event.LatLng);
                }
                infow.open(map, mark);
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    function buscaPatente(id) {
        muestraVehiculo(id);
        $('#listado_grupos .panel-collapse').collapse('show');
        $(".nombreclase").css("background-color", "white");
        $("#nombre_" + id).css("background-color", "yellow");
        var aTag = $("a[name='p_" + id + "']");
        $('#listados').animate({scrollTop: aTag.offset().top}, 'slow');
    }

    function muestraVehiculo(id) {
        var dataVehiculo = vehiculosArray[id];
        if (dataVehiculo.device.latest.loc.lat != undefined) {
            var marcador = markersArray[id];
            if (marcador.position != undefined) {
                map.panTo(marcador.position);
                map.setZoom(16);
                new google.maps.event.trigger(marcador, 'click');
            }
        } else {
            return false;
        }
    }

    function muestraVehiculoSinPos(id) {
        var dataVehiculo = vehiculosArray[id];
        var marcador = markersArray[id];
        if (marcador.position != undefined) {
            // console.log(marcador.position);
        }
    }

    function muestraVehiculoSolo(id) {
        var dataVehiculo = vehiculosArray[id];
        if (dataVehiculo.device.latest.loc.lat != undefined) {
            var marcador = markersArray[id];
            if (marcador.position != undefined) {
                map.panTo(marcador.position);
                map.setZoom(16);
            }
        } else {
            return false;
        }
    }

    function enviaComandos(id) {
        var dataVehiculo = vehiculosArray[id];
        $("#comandos").html("");
        var modelStr = '';
        var modelArray = [];
        var header = '<div style="width:auto;margin:10px;padding:5px;text-align:center">' +
                '<legend>Envio de Comandos</legend>' +
                '<p style="">' + dataVehiculo.name + '</p>' +
                '</div>';
        var cortacorriente = '<table border="0" cellspacing="2" cellpadding="2" class="table table-condensed" style="margin-bottom:5px;">' +
                '<thead><tr><th colspan="2" style="text-align:center">Corta Corriente</th></tr></thead>' +
                '<tbody><tr>' +
                '<td style="text-align: center;vertical-align: top">' +
                '<button type="button" onclick="cortacorrienteOff(' + dataVehiculo.id + ');" class="btn btn-danger btn-sm">STOP</button>' +
                '</td>' +
                '<td style="text-align: center;vertical-align: top">' +
                '<button type="button" onclick="cortacorrienteOn(' + dataVehiculo.id + ');" class="btn btn-success btn-sm">GO</button>' +
                '</td></tr></tbody></table>';
        var paradasegura = '<table border="0" cellspacing="2" cellpadding="2" class="table table-condensed" style="margin-bottom:5px;">' +
                '<thead><tr><th colspan="2" style="text-align:center">Parada Segura</th></tr></thead>' +
                '<tbody><tr>' +
                '<td style="text-align: center;vertical-align: top">' +
                '<button type="button" onclick="paradaseguraOff(' + dataVehiculo.id + ');" class="btn btn-danger btn-sm">OFF</button>' +
                '</td>' +
                '<td style="text-align: center;vertical-align: top">' +
                '<button type="button" onclick="paradaseguraOn(' + dataVehiculo.id + ');" class="btn btn-success btn-sm">ON</button>' +
                '</td></tr></tbody></table>';
        var chapa = '<table border="0" cellspacing="2" cellpadding="2" class="table table-condensed" style="margin-bottom:5px;">' +
                '<thead><tr><th colspan="2" style="text-align:center">Chapa Randómica</th></tr></thead>' +
                '<tbody><tr>' +
                '<td style="text-align: center;vertical-align: top">' +
                '<button type="button" onclick="chapaOff(' + dataVehiculo.id + ');" class="btn btn-danger btn-sm">OFF</button>' +
                '</td>' +
                '<td style="text-align: center;vertical-align: top">' +
                '<button type="button" onclick="chapaOn(' + dataVehiculo.id + ');" class="btn btn-success btn-sm">ON</button>' +
                '</td></tr></tbody></table>';
        var na = '<table border="0" cellspacing="2" cellpadding="2" class="table table-condensed" style="margin-bottom:5px;">' +
                '<thead><tr><th colspan="2" style="text-align:center">ENVIO DE COMANDOS NO HABILITADO</th></tr></thead>' +
                '<tbody><tr>' +
                '<td style="text-align: center;vertical-align: top" colspan="2">' +
                'CAMPO MODEL VACIO' +
                '</td></tr></tbody></table>';
        var boton_cerrar = '<hr/><div style="text-align:center"><button type="button" class="btn btn-danger" onclick="cierraBotones();">Cerrar</button></div>';
        var botones = '';
        if (dataVehiculo.info.model != undefined) {
            modelStr = dataVehiculo.info.model;
            modelArray = modelStr.split(",");
            $(modelArray).each(function (index) {
                if (this.toUpperCase() == "CC") {
                    botones = botones + cortacorriente;
                }
                if (this.toUpperCase() == "PS") {
                    botones = botones + paradasegura;
                }
                if (this.toUpperCase() == "CHAPA") {
                    botones = botones + chapa;
                }
            });
        }
        if (botones == "") {
            botones = na;
        }
        var contenido = header + botones + boton_cerrar;
        $("#comandos").html(contenido);
        $("#comando_td").show("slow");
        muestraVehiculoSolo(id);
    }

    function cierraBotones() {
        $("#comandos").html("");
        $("#comando_td").hide("slow");
    }


    function cortacorrienteOn(id) {
        var dataVehiculo = vehiculosArray[id];
        var confirmar = confirm("Seguro(a) de enviar comando SSSU020 a vehiculo " + dataVehiculo.name + " ?");
        if (confirmar == false) {
            return false
        }
        var comando = '>SSSU020<';
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/envia-comando'; ?>",
            dataType: "html",
            data: {id: id, comando: comando},
            success: function (response) {
                var responseJson = JSON.parse(response);
                if (responseJson.msg != undefined) {
                    alert("Comando enviado - MSG [" + responseJson.msg + "]");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });

    }

    function cortacorrienteOff(id) {
        var dataVehiculo = vehiculosArray[id];
        var confirmar = confirm("Seguro(a) de enviar comando SSSU021 a vehiculo " + dataVehiculo.name + " ?");
        if (confirmar == false) {
            return false
        }
        var comando = '>SSSU021<';
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/envia-comando'; ?>",
            dataType: "html",
            data: {id: id, comando: comando},
            success: function (response) {
                var responseJson = JSON.parse(response);
                if (responseJson.msg != undefined) {
                    alert("Comando enviado - MSG [" + responseJson.msg + "]");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });

    }

    function paradaseguraOn(id) {
        // SSSXP11
        var dataVehiculo = vehiculosArray[id];
        var confirmar = confirm("Seguro(a) de enviar comando SSSXP11 a vehiculo " + dataVehiculo.name + " ?");
        if (confirmar == false) {
            return false
        }
        var comando = '>SSSXP11<';
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/envia-comando'; ?>",
            dataType: "html",
            data: {id: id, comando: comando},
            success: function (response) {
                var responseJson = JSON.parse(response);
                if (responseJson.msg != undefined) {
                    alert("Comando enviado - MSG [" + responseJson.msg + "]");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
    }

    function paradaseguraOff(id) {
        // SSSXP10
        var dataVehiculo = vehiculosArray[id];
        var confirmar = confirm("Seguro(a) de enviar comando SSSXP10 a vehiculo " + dataVehiculo.name + " ?");
        if (confirmar == false) {
            return false
        }
        var comando = '>SSSXP10<';
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/envia-comando'; ?>",
            dataType: "html",
            data: {id: id, comando: comando},
            success: function (response) {
                var responseJson = JSON.parse(response);
                if (responseJson.msg != undefined) {
                    alert("Comando enviado - MSG [" + responseJson.msg + "]");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
    }

    function chapaOn(id) {
        // SSSU071
        var dataVehiculo = vehiculosArray[id];
        var confirmar = confirm("Seguro(a) de enviar comando SSSU071 a vehiculo " + dataVehiculo.name + " ?");
        if (confirmar == false) {
            return false
        }
        var comando = '>SSSU071<';
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/envia-comando'; ?>",
            dataType: "html",
            data: {id: id, comando: comando},
            success: function (response) {
                var responseJson = JSON.parse(response);
                if (responseJson.msg != undefined) {
                    alert("Comando enviado - MSG [" + responseJson.msg + "]");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
    }

    function chapaOff(id) {
        // SSSU070
        var dataVehiculo = vehiculosArray[id];
        var confirmar = confirm("Seguro(a) de enviar comando SSSU070 a vehiculo " + dataVehiculo.name + " ?");
        if (confirmar == false) {
            return false
        }
        var comando = '>SSSU070<';
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/envia-comando'; ?>",
            dataType: "html",
            data: {id: id, comando: comando},
            success: function (response) {
                var responseJson = JSON.parse(response);
                if (responseJson.msg != undefined) {
                    alert("Comando enviado - MSG [" + responseJson.msg + "]");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
    }
    
    
    // **************** comandos de forzar registro
    function quitarDePendientes(id){
        var newPendientes = [];
        if (pendientes.length > 0){
            $.each(pendientes, function(key,value){
                    if (key != id){
                        newPendientes[key] = value;
                    }
            });
            pendientes = newPendientes;
        }
    }
    
    function cuentaPendientes(){
        var res = 0;
        if (pendientes.length > 0){
            $.each(pendientes, function(key,value){
                    if (value != "" && value > 0 && value!=undefined){
                        res++;
                    }
            });
        }
        return res;
    }
    
    function setEspera(estado,id){
        var iconoVerde = '<i style="color:green" class="fa fa-sync-alt"></i>';
        var iconoAmarillo = '<i style="color:orange" class="fa fa-sync-alt fa-spin"></i>';
        if (estado == 2){
            $("#forzar_"+id).html(iconoAmarillo);
        }
        if (estado == 1){
            $("#forzar_"+id).html(iconoVerde);
            // pongo ese id en procesados
            procesados.push(id); 
        }
        // es un vehiculo que no deja de estar en amarillo
        if (estado == 3){
            $("#forzar_"+id).html(iconoVerde);
            quitarDePendientes(id);
        }
    }
    
    function estaProcesado(id){
        var res = false;
        $.each(procesados, function(indice, valor){
                if (id == valor){
                    res = true; 
                    // console.log("----ID = " + id + "--PROCESADO TRUE");
                }
        });
        return res;
    }
    
    function estaPendiente(id){
        var res = false;
        if (pendientes.length > 0){
            $.each(pendientes, function(key,value){
                    if (key == id && value > 0 && value!=undefined && value!=""){
                        res = true;
                    }
            });
        }
        return res
    }
    
    function procesaCidPendientes(){
        if (pendientes.length > 0){
            $.each(pendientes, function(key,value){
                    if (value!=undefined){
                        // console.log(key + " cid = " + value);
                        // veo si ya fue procesado para no esperar mas response
                        if (estaProcesado(key) == false){
                            esperaResponse(key);
                        }
                    }
            });
        }
        if (procesados.length == cuentaPendientes()){
            procesaPendientes = 0;
            procesados = [];
            pendientes = [];
            // console.log("Ya no hay mas pendientes");
        }
    }
    
    
    function enviaRefresh(id) {
        var dataVehiculo = vehiculosArray[id];
        if (estaPendiente(id)==true){
            setEspera(3,id); // lo mando a sacar de los pendientes y ponemos icono verde
            // console.log("ya esta pendiente = "+id);
            return false;
        }
        /*var confirmar = confirm("Seguro(a) de forzar registro a vehiculo " + dataVehiculo.name + " ?");
        if (confirmar == false) {
            return false
        }*/
        var comando = '>SXACT002<';
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/envia-comando'; ?>",
            dataType: "html",
            data: {id: id, comando: comando},
            success: function (response) {
                setEspera(2,id);
                var responseJson = JSON.parse(response);
                // console.log("---- ENVIOOOO --- "+comando);
                // console.log(responseJson);
                if (responseJson.msg != undefined) {
                    // console.log(id + " -- mensaje = "+responseJson.msg);
                    // alert("Comando enviado - MSG [" + responseJson.msg + "]");
                }
                if (responseJson.cid != undefined) {
                    // console.log("Se envia a procesar ID = "+id + "-- CID = "+responseJson.cid);
                    pendientes[id] = responseJson.cid;
                    procesaPendientes = 1;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
    }    
    
    function esperaResponse(id) {
        var cid = pendientes[id]!=undefined ? pendientes[id] : "";
        if (cid==""){return false;}
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/espera-response'; ?>",
            dataType: "html",
            data: {id: id, cid: cid},
            success: function (response) {
                var responseJson = JSON.parse(response);
                // console.log("----------- RESPONSE ----- ");
                // console.log(responseJson);
                if (responseJson.message!=undefined){
                    // console.log(id + "--  mensaje = " + responseJson.message);
                    if (responseJson.message == 'command not found'){
                        return false;
                    }
                    if (responseJson.message != 'command not found'){
                        // RXACT002
                        setEspera(1,id);
                        loadDataRefresh();
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
    }
    




</script>