<?php

use yii\helpers\Html;
use app\assets\AppAsset;
use app\assets\View;
use kartik\select2\Select2;
use kartik\widgets\SwitchInput;
use yii\bootstrap\Collapse;

$pagina = $this->context->route;
$imagen = "";
$session = Yii::$app->session;
$idUsuario = $session['IdUsuario'];
$nombreUsuario = $session['nombreUsuario'];

AppAsset::register($this);
Yii::$app->engine->getEventosArray();
Yii::$app->engine->getPatentesArray();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <link rel="shortcut icon" href="<?= Yii::getAlias('@web'); ?>/images/favicon.png" type="image/x-icon"/>

        <?php $this->head() ?>
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/css/mantenedor.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/content/fontAwesome/fa/all.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <?php $this->beginBody() ?>


        <!-- Begin page -->
        <div id="wrapper" class="forced">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="ml-3">
                        <a href="#" style="text-decoration:none;" class="logo">
                            <img src="<?= Yii::getAlias('@web'); ?>/images/gps.png" class="logo-min" width="50" alt="">
                            <span><img src="<?= Yii::getAlias('@web'); ?>/images/logo.png" width="150" alt=""></span>
                        </a>
                    </div>
                </div>
                <!-- Button mobile view to collapse sidebar menu -->
                <nav class="navbar-custom">
                    <ul class="list-inline float-right mb-0" style="background: rgba(0, 0, 0, 0.2) !important;">
                        <li class="list-inline-item dropdown notification-list">
                            <?php
                            if ($imagen == "") {
                                $imagen = Yii::getAlias('@web') . "/images/icon_user.svg";
                            }
                            ?>
                            <a class="nav-link dropdown-toggle  nav-user " data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="<?= $imagen ?>" alt="user" class="rounded-circle p-1">
                                <strong style="font-weight: bolder; color:white; font-size:.8em"> <?php echo $nombreUsuario; ?></strong>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                <a href="<?= Yii::getAlias('@web'); ?>/login/cerrarsesion" class="dropdown-item notify-item">
                                    <i class="mdi mdi-logout"></i> <span>Cerrar Sesion</span>
                                </a>
                            </div>
                        </li>
                    </ul>
                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left waves-light waves-effect boton_mdi">
                                <i class="mdi mdi-menu"></i>
                            </button>
                        </li>
                        <li class="float-left">
                            <span style="width:60px;color:white;font-weight: bold;">
                                <br/>
                                <span style="margin-left:40px;color:white;font-weight: bold;">Hora Actual</span>
                                <br/>
                                <span id="time_str" style="margin-left:50px;width:60px;color:white;font-weight: bold;font-size: 16px;"></span>
                            </span>
                        </li>
                        <li class="float-left">
                            <br/>
                            <span onclick="toggleFullScreen();" style="cursor:pointer;margin-left:40px;color:white;" title="Vista pantalla completa">
                                <i class="fa fa-2x fa-expand"></i>
                            </span>
                        </li>
                        <li class="float-left">
                            <br/>
                            <?= Html::a('<i class="fa fa-2x fa-home"></i>', ['/site/index'], ['title' => 'Inicio', 'style' => 'margin-left:10px;color:white']); ?>
                        </li>
                        <li class="float-left">
                            <br/>
                            <?= Html::a('<i class="fa fa-2x fa-history"></i>', ['/reportes/historico'], ['title' => 'Reporte Historico', 'style' => 'margin-left:10px;color:white']); ?>
                        </li>

                        <li class="float-left">
                            <br/>
                            <?= Html::a('<i class="fa fa-2x fa-traffic-light"></i>', ['/reportes/flota'], ['title' => 'Resumen de Flota', 'style' => 'margin-left:10px;color:white']); ?>
                        </li>
                        <li class="float-left">
                            <br/>
                            <?= Html::a('<i class="fa fa-2x fa-check-double"></i>', ['/reportes/eventos'], ['title' => 'Resumen de Eventos', 'style' => 'margin-left:10px;color:white', 'onclick'=>'window.open(this.href,"Eventos","width=1024,height=700"); return false;']); ?>
                        </li>
                        <li class="float-left">
                            <br/>
                            <?= Html::a('<i class="fa fa-2x fa-map"></i>', ['/reportes/mosaico'], ['title' => 'Mosaico posicion de vehiculos en Mapa', 'style' => 'margin-left:10px;color:white', 'onclick'=>'window.open(this.href,"Mosaico Vehiculos","width=1024,height=700"); return false;']); ?>
                        </li>

                        <li class="float-left">
                            <br/>
                            <?= Html::a('<i class="fa fa-2x fa-question-circle"></i>', ['/reportes/ayuda'], ['title' => 'Ayuda referencial', 'style' => 'margin-left:150px;color:white']); ?>
                        </li>

                    </ul>
                </nav>
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->

            <div class="left side-menu">
                <div class="sidebar-inner">
                    <!--- Divider -->
                    <div id="sidebar-menu" class="pt-0">
                        <ul>

                            <?php if ($idUsuario == 1) { ?>
                                <li class="<?= $pagina == "gruposusuarios/lista" ? "active" : "" ?>">
                                    <a href="<?= Yii::getAlias('@web'); ?>/gruposusuarios/lista" class="waves-effect waves-primary">
                                        <i class="fal fa-plus"></i><span> Grupos/Usuarios </span>
                                    </a>
                                </li>

                                <li class="<?= $pagina == "usuarios/lista" ? "active" : "" ?>">
                                    <a href="<?= Yii::getAlias('@web'); ?>/usuarios/lista" class="waves-effect waves-primary">
                                        <i class="fal fa-users"></i><span> Usuarios </span>
                                    </a>
                                </li>  
                            <?php } ?>

                            <?php if ($idUsuario != 1) { ?>
                                <!--
                                <li class="<?= $pagina == "inicio" ? "active" : "" ?>">
                                    <a href="<?= Yii::getAlias('@web'); ?>/site/index" class="waves-effect waves-primary">
                                        <i class="fal fa-home"></i><span> Inicio </span>
                                    </a>
                                </li>

                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect waves-primary <?= $pagina == "usuario-crear" ? "active" : "" ?>">
                                        <i class="fal fa-user-friends"></i><span> Reportes </span>
                                        <span class="menu-arrow"></span> 
                                    </a>
                                    <ul class="list-unstyled" style="font-size:14px;">
                                        <li <?= $pagina == "usuario-crear" ? "active" : "" ?>>
                                            <a href="<?= Yii::getAlias('@web'); ?>/reportes/historico" class="waves-effect waves-primary <?= $pagina == "usuario-crear" ? "active" : "" ?>">
                                                <i class="fal fa-history"></i><span> Historico</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                -->
                            <?php } ?>
                        </ul>
                        <div class="clearfix"></div>
                        <div style="padding:10px;" id="header_listado">
                            <div style="text-align:left;padding:3px;">
                                <span><b>Vehículos</b>&nbsp;</span><span id="cantidad_vehiculos" class="badge badge-danger"></span>
                            </div>
                            <div style="display: inherit">
                                <label class="control-label">Mostrar Patente o Muestra</label>
                                <div  style="margin-bottom:8px;">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-success active" style="width:150px;" onclick="setMostrar(1);">
                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Patente
                                        </label>
                                        <label class="btn btn-success" style="width:150px;" onclick="setMostrar(2);">
                                            <input type="radio" name="options" id="option2" autocomplete="off"> Muestra
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group" id="patente_div">
                                    <?php
                                    echo Select2::widget([
                                        'name' => 'patente',
                                        'data' => Yii::$app->engine->getPatentesArrayLista(),
                                        'options' => [
                                            'placeholder' => 'Seleccione Patente..',
                                            'multiple' => false,
                                            'id' => 'patente',
                                            'class' => 'form-control',
                                            'style' => 'border:1em solid red;font-size:12px',
                                        ],
                                        'pluginEvents' => [
                                            "change" => "function() { buscaPatente(this.value); }",
                                        ],
                                        'pluginOptions' => [
                                            'dropdownCssClass' => 'listado_patentes',
                                            'allowClear' => false,
                                            'theme' => 'classic',
                                        ],
                                    ]);
                                    ?>  
                                </div>
                            </div>
                            <div style="margin-bottom:5px;">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-warning btn-sm" style="width:150px;" onclick="$('#listado_grupos .panel-collapse').collapse('show');">
                                        <input type="radio" name="options" id="option1" autocomplete="off" checked> Expandir
                                    </label>
                                    <label class="btn btn-warning btn-sm active" style="width:150px;" onclick="$('#listado_grupos .panel-collapse').collapse('hide');">
                                        <input type="radio" name="options" id="option2" autocomplete="off"> Contraer
                                    </label>
                                </div>
                            </div>
                            <div id="listados" style="vertical-align:top;width:auto;height:450px;overflow-x:hidden;overflow-y: scroll"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">

                <!-- Start content -->
                <div class="content" style="">
                    <div class="container-fluid">
                        <?= $content ?>
                    </div>
                    <!-- end container -->
                </div>
                <!-- end content -->

                <footer class="footer">
                    <?= date("Y") ?> <span class="hide-phone">- Bermann </span>
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        <script>
            var resizefunc = [];
        </script>

        <!-- Plugins  -->

        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/detect.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/fastclick.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/jquery.slimscroll.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/jquery.blockUI.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/waves.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/wow.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/switchery/switchery.min.js"></script>
        <!-- Custom main Js -->
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/jquery.app.js"></script>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
<script type="text/javascript">
            $(".boton_mdi").click(function () {
                $("#header_listado").toggle();
            });

            function ponerFecha() {
                var today = new Date();
                var segundos = today.getSeconds() < 10 ? '0' + today.getSeconds() : today.getSeconds();
                var minutos = today.getMinutes() < 10 ? '0' + today.getMinutes() : today.getMinutes();
                var time = today.getHours() + ":" + minutos + ":" + segundos;
                $("#time_str").html(time);
            }

            $(document).ready(function () {
                setInterval(function () {
                    ponerFecha();
                },
                        1000);
            });


            function toggleFullScreen() {
                var elem = document.body;
                // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
                if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
                    if (elem.requestFullScreen) {
                        elem.requestFullScreen();
                    } else if (elem.mozRequestFullScreen) {
                        elem.mozRequestFullScreen();
                    } else if (elem.webkitRequestFullScreen) {
                        elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                    } else if (elem.msRequestFullscreen) {
                        elem.msRequestFullscreen();
                    }
                } else {
                    if (document.cancelFullScreen) {
                        document.cancelFullScreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitCancelFullScreen) {
                        document.webkitCancelFullScreen();
                    } else if (document.msExitFullscreen) {
                        document.msExitFullscreen();
                    }
                }
            }

</script>   
<style>
    #patente_div .select2-container {
        font-size: 12px;
        border: 1px solid gray;
    }
    #patente_div .select2-selection__rendered {
        font-size: 12px;
        padding-left: 5px;
        margin-left: -14px;
        margin-top: -8px;
        width:170px;
    }

    .listado_patentes {
        font-size:12px;
    }

    .panel-body {
        padding: 5px;
    }

    .itemlista_nombre{
        text-align:left;
        width:150px;
        max-width: 150px;
        float:left;
    }

    .itemlista_icon{
        text-align:left;
        padding-top:5px;
        /*padding-bottom:5px;*/
        width:80px;
        max-width: 80px;
        height:25px;
        float:left;
        /*border:1px solid red;*/
    }

    .itemlista_fecha{
        text-align:center;
        padding:0px;
        width:40px;
        max-width: 40px;
        height:25px;
        float:left;
        /*border:1px solid red;*/
    }

    .panel-heading {
        padding: 7px;
        border-bottom: 1px solid lightgray;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
    }

    .topbar .topbar-left {
        float: left;
        position: relative;
        width: 330px;
        z-index: 1;
        background-color: #3bafda;
        max-height: 70px;
    }

    .circulo_rojo {
        border-radius: 50%;
        width: 20px;
        height: 20px;
        margin-left: 30%;
        background: red;
        border: 1px solid black;
    }

    .circulo_verde {
        border-radius: 50%;
        width: 20px;
        height: 20px;
        margin-left: 30%;
        background: green;
        border: 1px solid black;
    }

    .circulo_gris {
        border-radius: 50%;
        width: 20px;
        height: 20px;
        margin-left: 30%;
        background: lightgray;
        border: 1px solid black;
    }

    .badge {
        font-weight: 600;
        padding: 4px;
        font-size: 14px;
        margin-bottom: 5px;
    }

</style>    
