<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<?php $this->beginPage() ?>
<?php $this->beginBody() ?>

<!DOCTYPE html>
<html style="background-color: #aaba62">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="<?= Yii::getAlias('@web'); ?>/images/favicon.png" type="image/x-icon"/>

        <title>Online - Inicio sesion</title>

        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <link href="<?= Yii::getAlias('@web'); ?>/content/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/content/fontAwesome/fa/all.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::getAlias('@web'); ?>/content/admin/css/style.css" rel="stylesheet" type="text/css">

        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/modernizr.min.js"></script>

    </head>
    <body style="background-color: #aaba62" class="h-100">

        
        <div class="pt-5">
            <br><br><br>
            <div class="text-center pt-5 ">
                <img src="<?= Yii::getAlias('@web'); ?>/images/logo2x.png" width="300" alt="">
            </div>

            <div class="wrapper-page pt-5 mt-0">

                <div class=" card-box">
                    <div class="text-center">
                        <a href="#" class="logo-lg d-none"></i> <img src="<?= Yii::getAlias('@web'); ?>/images/dct.png" alt="" width="300"> </a>
                        
                        <?php if (isset($noLogin)) { ?>
                            <span class="badge badge-danger mb-3">Usuario o clave invalido</span>
                        <?php } ?>
                    </div>


                    <?php $form = ActiveForm::begin([
                            'method' => 'post', 
                            'id'=> 'InicioSesionForm', 
                            'options'=> [
                                
                            ], 
                            'enableClientValidation' => false,
                            'enableAjaxValidation' => true, 
                            ]); ?>

                            <?= $form->field($model, 'Usuario', [
                            'template' => '<div class="form-group row mb-0">
                                            <div class="col-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                                                    </div>
                                                {input}
                                                </div>
                                            </div>
                                        </div>
                                        {error}{hint}',
                                'errorOptions'=>['class'=>'badge badge-danger']
                            ])->textInput(['class'=>'form-control', 'placeholder' => 'Usuario'])
                            ->label(false) ?>

                            <?= $form->field($model, 'Clave', [
                            'template' => '<div class="form-group row mb-0">
                                            <div class="col-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-lock"></i></span>
                                                    </div>
                                                {input}
                                                </div>
                                            </div>
                                        </div>
                                        {error}{hint}',
                                'errorOptions'=>['class'=>'badge badge-danger']
                            ])->textInput(['class'=>'form-control', 'type' => 'password', 'placeholder' => 'Clave'])
                            ->label(false) ?>


                        <div class="form-group text-right m-t-20">
                            <div class="col-xs-12">
                                <?= Html::submitButton(Yii::t('app', '<i class="fas fa-sign-in"></i> Entrar'), ['class' => 'btn btn-success btn-custom w-md waves-effect waves-light']) ?>
                            </div>
                        </div>


                        <?php $form->end(); ?>

                </div>
                
            </div>
        </div>                  

        <script>
            var resizefunc = [];
        </script>


<?php $this->endBody() ?>
<?php $this->endPage() ?>


        <!-- Plugins  -->
        
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/popper.min.js"></script><!-- Popper for Bootstrap -->
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/bootstrap.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/detect.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/fastclick.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/jquery.slimscroll.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/jquery.blockUI.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/waves.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/wow.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/jquery.nicescroll.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/jquery.scrollTo.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/plugins/switchery/switchery.min.js"></script>

        <!-- Custom main Js -->
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/jquery.core.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/content/admin/js/jquery.app.js"></script>
	
	</body>
</html>