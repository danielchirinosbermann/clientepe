<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\export\ExportMenu;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;

$this->title = 'Reportes - Resumen de Flota';
$bread1 = 'Resumen de Flota';
$bread2 = $this->title;
$bread3 = '';
$this->params['activeLink'] = "reporte-flota";


ini_set('max_execution_time', '99600');
ini_set('max_input_time', '-1');
ini_set('memory_limit', '-1');
set_time_limit(9600);

$imagenFlechaVerde = '<img src="' . Yii::getAlias('@web') . '/images/flecha_verde.png" style="width:16px;height:16px;" />';
$imagenFlechaAmarilla = '<img src="' . Yii::getAlias('@web') . '/images/flecha_amarilla.png" style="width:16px;height:16px;" />';
$imagenFlechaRoja = '<img src="' . Yii::getAlias('@web') . '/images/flecha_roja.png" style="width:16px;height:16px;" />';
$imagenFlechaStop = '<img src="' . Yii::getAlias('@web') . '/images/stop_1.png" style="width:16px;height:16px;" />';
$imagenFlechaStop2 = '<img src="' . Yii::getAlias('@web') . '/images/stop_2.png" style="width:16px;height:16px;" title="Vehiculo sin información" />';
$imagenFlechaStop3 = '<img src="' . Yii::getAlias('@web') . '/images/stop_3.png" style="width:16px;height:16px;" title="Vehiculo sin información" />';



?>

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h2 class="page-title" style="font-size: 16px;"><?= $this->title ?></h2>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php
Yii::$app->engine->getEventosArray();

function getAddress($lat, $lon) {
    $googleMapsUrl = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $lat . ',' . $lon . '&key=AIzaSyCeWLXP10h_pemnBj0gYDp_9-wICMqeR9I';
    ini_set("curl.cainfo", null);
    $timeout = 60;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $googleMapsUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $data = curl_exec($ch);
    $err = curl_error($ch);
    curl_close($ch);
    $direccion = '';
    if ($err) {
        $dataArray = "Error: " . $err;
    } else {
        $dataArray = json_decode($data, true);
    }
    if (is_array($dataArray) == true) {
        if (isset($dataArray["results"][0]["formatted_address"]) == true) {
            $direccion = $dataArray["results"][0]["formatted_address"];
        }
    }
    return $direccion;
}

function dataProvider() {
    $dataSourceFlota = array();
    $imagenFlechaVerde = '<img src="' . Yii::getAlias('@web') . '/images/flecha_verde.png" style="width:16px;height:16px;" />';
    $imagenFlechaAmarilla = '<img src="' . Yii::getAlias('@web') . '/images/flecha_amarilla.png" style="width:16px;height:16px;" />';
    $imagenFlechaRoja = '<img src="' . Yii::getAlias('@web') . '/images/flecha_roja.png" style="width:16px;height:16px;" />';
    $imagenFlechaStop = '<img src="' . Yii::getAlias('@web') . '/images/stop_1.png" style="width:16px;height:16px;" />';
    $imagenFlechaStop2 = '<img src="' . Yii::getAlias('@web') . '/images/stop_2.png" style="width:16px;height:16px;" title="Vehiculo sin información" />';
    $imagenFlechaStop3 = '<img src="' . Yii::getAlias('@web') . '/images/stop_3.png" style="width:16px;height:16px;" title="" />';
    $vehiculos = Yii::$app->engine->getPatentesDataArrayCSV();
    $dataArray = Yii::$app->engine->getDatosFlota($vehiculos);
    if (is_array($dataArray) == true && count($dataArray) > 0) {
        foreach ($dataArray as $patentes) {
            if (is_array($patentes) == true) {
                $contador = 0;
                foreach ($patentes as $patente) {
                    $imagenFlecha = '';
                    $id = $patente["id"];
                    $flecha = '';
                    $patenteName = '';
                    $patenteMuestra = $patente["name"];
                    $grupo = '';
                    $evento = '';
                    $fecha = '';
                    $velocidad = '';
                    $lat = '';
                    $lon = '';
                    $direccion = '';
                    $detenidoValor = '';
                    $detenidoLabel = '';

                    if (isset($patente["device"]["latest"]["loc"]["mph"]) == true) {
                        $velocidadKm = round(intval($patente["device"]["latest"]["loc"]["mph"]) * 1.609) . ' Km/h';
                        $velocidad = isset($patente["device"]["latest"]["loc"]["mph"]) == true ? intval($patente["device"]["latest"]["loc"]["mph"]) : 0;
                    }

                    // fecha , velocidad
                    if (isset($patente["device"]["latest"]["loc"]) == true) {

                        if (isset($patente["device"]["latest"]["loc"]["lat"]) == true) {
                            $lat = $patente["device"]["latest"]["loc"]["lat"];
                        }

                        if (isset($patente["device"]["latest"]["loc"]["lon"]) == true) {
                            $lon = $patente["device"]["latest"]["loc"]["lon"];
                        }

                        if ($lat != "" && $lon != "") {
                            $direccion = getAddress($lat, $lon);
                        }

                        $vArray = $patente["device"]["latest"]["loc"];
                        $tiempo = isset($vArray["evtime"]) == true ? $vArray["evtime"] : "";
                        if ($tiempo != "") {
                            $fecha = date('d/m/Y H:i:s', $tiempo);
                        }
                    }

                    $imagenFlecha = '<span id="flecha_' . $id . '" data-patente="' . $id . '" class="patente_icon" style="margin-left:5px;cursor:pointer"></span>';;
                    $tiempo_detenido_minutos = '';

                    /*
                    if (isset($patente["device"]["latest"]["data"]["io_ign"]) == true) {
                        if ($patente["device"]["latest"]["data"]["io_ign"]["value"] == false) {
                            $datoprevio = $patente["device"]["latest"]["data"]["io_ign"]["change"]["evtime"];
                            $datoactual = $patente["device"]["latest"]["data"]["io_ign"]["evtime"];
                            $milliseconds = abs(intval($datoactual) - intval($datoprevio));
                            $seconds = floor($milliseconds / 1000);
                            $tiempo_detenido_minutos = floor($seconds / 60);
                        }

                        if ($tiempo_detenido_minutos > 0) {

                            if ($tiempo_detenido_minutos > 0 && $tiempo_detenido_minutos <= 5 && $velocidad < 5) {
                                $imagenFlecha = $imagenFlechaAmarilla;
                            }
                            if ($tiempo_detenido_minutos > 5 && $tiempo_detenido_minutos <= 25 && $velocidad < 5) {
                                $imagenFlecha = $imagenFlechaRoja;
                            }
                            if ($tiempo_detenido_minutos > 25 && $tiempo_detenido_minutos <= 2900 && $velocidad < 5) {
                                $imagenFlecha = $imagenFlechaStop;
                            }
                            if ($tiempo_detenido_minutos > 2880 && $velocidad < 5) {
                                $imagenFlecha = $imagenFlechaStop3;
                            }
                        }
                    } else {
                       $imagenFlecha = $imagenFlechaVerde;
                    }

                    if ($velocidad > 5) {
                        $imagenFlecha = $imagenFlechaVerde;
                    }
                    if ($fecha == '') {
                        $imagenFlecha = $imagenFlechaStop2;
                    }
                    */




                    if (isset($patente["info"]["alias"]) == true) {
                        $grupo = $patente["info"]["alias"];
                    }

                    if (isset($patente["info"]["license_plate"]) == true) {
                        $patenteName = $patente["info"]["license_plate"];
                    }

                    if (isset($patente["device"]["latest"]["loc"]["code"]) == true) {
                        $evento = $patente["device"]["latest"]["loc"]["code"] > 0 ? Yii::$app->engine->getEventoName($patente["device"]["latest"]["loc"]["code"]) : "";
                    }
                    $contador++;
                    $dataSourceFlota[] = array(
                        'id' => $contador,
                        'flecha' => $imagenFlecha,
                        'patente_id' => $id,
                        'fecha' => $fecha,
                        'velocidad' => $velocidad,
                        'grupo' => $grupo,
                        'muestra' => $patenteMuestra,
                        'patente' => $patenteName,
                        'evento' => $evento,
                        'lat' => $lat,
                        'lon' => $lon,
                        'direccion' => $direccion
                    );
                    // echo $imagenFlecha . '  -----' . $id . '----> ' . $fecha . '----' . $velocidad . '----' . $grupo . '----' . $patenteMuestra . '----' . $patenteName . '----' . $evento . '----[' . $lat . ']-[' . $lon . ']----- ' . $direccion . ' -- [' . $detenidoValor . '] <hr/>';
                }
            }
        }
    }

    return new ArrayDataProvider([
        'allModels' => $dataSourceFlota,
        'pagination' => [
            'pageSize' => 300,
        ],
        'sort' => [
            'defaultOrder' => 'grupo asc, patente asc, fecha desc',
            'attributes' => ['id', 'patente', 'muestra', 'fecha', 'velocidad', 'evento'],
        ],
    ]);
}
?>

<?php
$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    'patente',
    'muestra',
    'evento',
    'velocidad',
    [
        'label' => 'Fecha',
        'attribute' => 'fecha',
        'format' => 'html',
    ],
    'direccion',
];

$datos = dataProvider();
$totalDatos = $datos->getTotalCount();

$descargarLink = ExportMenu::widget([
            'dataProvider' => $datos,
            'columns' => $gridColumns,
            'asDropdown' => false,
            'template' => '{menu}',
            'showConfirmAlert' => false,
            'showColumnSelector' => false,
            'filename' => 'registrosExportadosOnline',
            'fontAwesome' => false,
            'exportConfig' => [
                'Html' => false,
                'Csv' => false,
                'Txt' => false,
                'Pdf' => false,
                'Xls' => false,
                'Xlsx' => [
                    'label' => 'Excel'
                ],
            ],
        ]);
?>
<?php
if ($totalDatos > 0) {
    echo '<div class="col-md-6 alert alert alert-danger" style="font-size:14px;"><label>Descargar Datos</label>' . $descargarLink . '</div>';
}
?>
<div id="mensajes" style="padding:5px;text-align:center;display:none"></div>
<div class="card-box table-responsive">  
    <?php Pjax::begin(['timeout' => false]); ?>
    <?=
    GridView::widget([
        'id' => 'registros',
        'dataProvider' => $datos,
        'tableOptions' => ['class' => 'table', 'style' => 'width:100%;font-size:14px;'],
        'responsive' => true,
        'responsiveWrap' => true,
        'bordered' => true,
        'striped' => false,
        'bootstrap' => true,
        'pjaxSettings' => [
            'loadingCssClass' => true,
        ],
        /* 'floatHeader' => true, */
        /* 'floatHeaderOptions' => ['top' => '50'], */
        'hover' => true,
        'pjax' => true,
        'columns' => [
            [
                'label' => 'No.',
                'attribute' => 'id',
                'format' => 'html',
                'options' => ['style' => 'width:15px;max-width:15px;'],
            ],
            [
                'label' => 'Estado',
                'attribute' => 'flecha',
                'format' => 'raw',
                'options' => ['style' => 'width:15px;max-width:15px;'],
            ],
            'patente',
            'muestra',
            'evento',
            [
                'label' => 'Fecha',
                'attribute' => 'fecha',
                'format' => 'html',
                'options' => ['style' => 'width:15%;max-width:15%;'],
            ],
            'velocidad',
            [
                'label' => 'Direccion',
                'attribute' => 'direccion',
                'format' => 'raw',
                'options' => ['style' => 'width:15%;max-width:15%;'],
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>

<script type="text/javascript">
    $("#patente_div, #header_listado").remove();
    
    var flecha_verde = '<?= $imagenFlechaVerde; ?>';
    var flecha_amarilla = '<?= $imagenFlechaAmarilla; ?>';
    var flecha_roja = '<?= $imagenFlechaRoja; ?>';
    var flecha_stop = '<?= $imagenFlechaStop; ?>';
    var flecha_stop2 = '<?= $imagenFlechaStop2; ?>';
    var flecha_stop3 = '<?= $imagenFlechaStop3; ?>';
    var vehiculos = [];
    
    
    function getTiempoDetenido(v){
       var res = "";
       if (v.device.latest.data.io_ign != undefined) {
           if (v.device.latest.data.io_ign.value == false){
               var datoprevio = v.device.latest.data.io_ign.change.evtime;
               var datoactual = v.device.latest.data.io_ign.evtime;
               var fechaPrevia = new Date(datoprevio*1000);
               var fechaActual = new Date(datoactual*1000);
               var diffMs = (fechaActual - fechaPrevia);
               var seg = diffMs / 1000;
               var diffMins = parseInt(seg / 60);
               res = diffMins;
           } else {
               res = 0;
           }
       }
       return res;
    }
    
    function actualizaFlechasListado() {
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/get-ulpos'; ?>",
            dataType: "html",
            async: true,
            success: function (response) {
                vehiculos = JSON.parse(response);
                if (vehiculos.length > 0) {
                    $.each(vehiculos, function (i, v) {
                        var flecha = flecha_verde;
                        var vehiculo = v.id;
                        var rotacion = v.device.latest.loc.head;
                        var velocidad = parseInt(v.device.latest.loc.mph * 1.6);
                        var fecha = v.device.latest.loc.evtime;
                        var detenido = 0;
                        
                        var tiempo_detenido_minutos = getTiempoDetenido(v);
                        
                        // verde
                        if (tiempo_detenido_minutos > 0 && tiempo_detenido_minutos <= 5 && velocidad < 5) {
                            flecha = flecha_amarilla;
                        }
                        // roja
                        if (tiempo_detenido_minutos > 5 && tiempo_detenido_minutos <= 25 && velocidad < 5) {
                            flecha = flecha_roja;
                        }
                        // stop rojo
                        if (tiempo_detenido_minutos > 25 && tiempo_detenido_minutos <= 2900 && velocidad < 5) {
                            flecha = flecha_stop;
                        }
                        // stop negro 
                        if (tiempo_detenido_minutos > 2900 && velocidad < 5) {
                            flecha = flecha_stop3;
                        }
                        
                        if (velocidad > 5){
                            flecha = flecha_verde;
                        }
                        if (fecha == undefined || fecha == '' || fecha == null){
                            flecha = flecha_stop2;
                        }
                        
                        $('.patente_icon').each(function(index){
                            var dataId = $(this).data('patente');
                            console.log(dataId+"----"+v.id);
                            if (dataId == v.id){
                                $(this).html(flecha);
                            }
                        });
                    });
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                // 
            }
        });
    }
    
    $(document).ready(function(){
        actualizaFlechasListado();
    });
    
</script>

<style>
    .summary {
        font-size: 14px;
        text-align: right;
    }

    .pagination {
        font-size: 14px;
    }

    #registros-grid-container thead{
        background-color: lightgray;
    }

    tbody .select2-container .select2-selection {
        height: 100px;
        overflow-y: scroll;
        overflow-x: hidden;
    } 

    .s2-togall-button{
        display:none;
    }

    .bootstrap-timepicker .inline-addon {
        display:none!important;
    }

    .kv-grid-loading {
        position: relative;
        overflow: hidden;
        max-height: 100%;
    }

    .kv-grid-loading .kv-loader {
        position: absolute;
        width: 100px !important;
        height: 100px !important;
        top: 20px!important;
        left: 50% !important;
    }
</style>