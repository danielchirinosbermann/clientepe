<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\export\ExportMenu;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;
use yii\helpers\Json;

$this->title = 'Ayuda y Referencias';
$bread1 = 'Ayuda';
$bread2 = $this->title;
$bread3 = '';
$this->params['activeLink'] = "reporte-ayuda";

ini_set('max_execution_time', '99600');
ini_set('max_input_time', '-1');
ini_set('memory_limit', '-1');
set_time_limit(9600);
?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h2 class="page-title" style="font-size: 16px;"><?= $this->title ?></h2>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<?php
$pagina1 = '<img src="' . Yii::getAlias('@web') . '/images/ayuda_1.png" style="width:80%;height:80%" />';
$pagina2 = '<img src="' . Yii::getAlias('@web') . '/images/ayuda_2.png" style="width:80%;height:80%" />';
$pagina3 = '<img src="' . Yii::getAlias('@web') . '/images/ayuda_3.png" style="width:80%;height:80%" />';
$pagina4 = '<img src="' . Yii::getAlias('@web') . '/images/ayuda_4.png" style="width:80%;height:80%" />';
$pagina5 = '<img src="' . Yii::getAlias('@web') . '/images/ayuda_5.png" style="width:80%;height:80%" />';
?>

<legend>Pagina Inicio</legend>
<div style="padding:10px;">
    <?= $pagina1; ?>
</div>
<hr/>
<legend>Reporte Historico</legend>
<div style="padding:10px;">
    <?= $pagina2; ?>
</div>
<hr/>
<legend>Resumen de Flota</legend>
<div style="padding:10px;">
    <?= $pagina3; ?>
</div>
<hr/>
<legend>Resumen de Eventos</legend>
<div style="padding:10px;">
    <?= $pagina4; ?>
</div>
<hr/>
<legend>Mosaico Vehículos</legend>
<div style="padding:10px;">
    <?= $pagina5; ?>
</div>
<hr/>
