<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyCeWLXP10h_pemnBj0gYDp_9-wICMqeR9I&libraries=geometry,places"></script>
<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\export\ExportMenu;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;

$this->title = 'Mosaico de posicion de vehiculos en el Mapa';
$bread1 = 'Posicion Vehiculos';
$bread2 = $this->title;
$bread3 = '';
$this->params['activeLink'] = "reporte-mosaico";


ini_set('max_execution_time', '99600');
ini_set('max_input_time', '-1');
ini_set('memory_limit', '-1');
set_time_limit(9600);

$eventosArray = Yii::$app->engine->getEventosArray();

?>

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h2 class="page-title" style="font-size: 16px;"><?= $this->title ?></h2>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php

function getAddress($lat, $lon) {
    $googleMapsUrl = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $lat . ',' . $lon . '&key=AIzaSyCeWLXP10h_pemnBj0gYDp_9-wICMqeR9I';
    ini_set("curl.cainfo", null);
    $timeout = 60;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $googleMapsUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $data = curl_exec($ch);
    $err = curl_error($ch);
    curl_close($ch);
    $direccion = '';
    if ($err) {
        $dataArray = "Error: " . $err;
    } else {
        $dataArray = json_decode($data, true);
    }
    if (is_array($dataArray) == true) {
        if (isset($dataArray["results"][0]["formatted_address"]) == true) {
            $direccion = $dataArray["results"][0]["formatted_address"];
        } else {
            $direccion = 'Error = ' . $data;
        }
    } else {
        $direccion = 'Error = ' . $data;
    }
    return $direccion;
}

$vehiculosStr = "";
if (isset($_GET['vehiculos']) == true && $_GET['vehiculos'] != "") {
    $vehiculosStr = $_GET['vehiculos'];
}
$data = Yii::$app->engine->getPatentesArray();
$vArray = $vehiculosStr != "" ? explode(',', $vehiculosStr) : "";
$dataPatentesFinal = array();
$contador = 0;
if ($data != null && is_array($data) == true) {
    foreach ($data as $vehiculoData) {
        $lat = '';
        $lon = '';
        $fecha = '';
        $patente = '';
        $velocidad = '';
        $imagenFlecha = '';
        $direccion = '';
        $head = '';
        $puedo = true;
        if ($vehiculosStr != "" && is_array($vArray) == true && isset($vehiculoData->id) == true && in_array($vehiculoData->id, $vArray) == false) {
            $puedo = false;
        }
        if (isset($vehiculoData->device->latest->loc->lat) == false || isset($vehiculoData->device->latest->loc->lon) == false) {
            $puedo = false;
        }
        if ($puedo == true) {
            $contador++;
            if (isset($vehiculoData->device->latest->loc->lat) == true && isset($vehiculoData->device->latest->loc->lon) == true) {
                $lat = $vehiculoData->device->latest->loc->lat;
                $lon = $vehiculoData->device->latest->loc->lon;
            }
            if (isset($vehiculoData->device->latest->loc->evtime) == true) {
                $fechaValor = $vehiculoData->device->latest->loc->evtime;
                if ($fechaValor != "") {
                    $fecha = date('d/m/Y H:i', $fechaValor);
                }
            }
            if (isset($vehiculoData->info->license_plate) == true) {
                $patente = $vehiculoData->info->license_plate;
            }
            if (isset($vehiculoData->device->latest->loc->mph) == true) {
                $velocidad = $vehiculoData->device->latest->loc->mph;
            }
            if ($lat != "" && $lon != "") {
                $direccion = getAddress($lat, $lon);
            }
            if (isset($vehiculoData->device->latest->loc->head) == true) {
                $head = $vehiculoData->device->latest->loc->head;
            }
            $ultimoEvento = '';
            if (isset($vehiculoData->device->latest->loc->code) == true) {
                $eventoId = $vehiculoData->device->latest->loc->code;
                $codeArray = explode(',', '30,31,44,45,22,23,3,4,6,7,53,58');
                if (in_array($eventoId, $codeArray) == true) {
                    $ultimoEvento = Yii::$app->engine->getEventoName($eventoId);
                }
            }

            $dataPatentesFinal[] = array(
                'id' => $contador,
                'evento' => $ultimoEvento,
                'patente_id' => $vehiculoData->id,
                'flecha' => '',
                'patente' => $patente,
                'muestra' => $vehiculoData->name,
                'lat' => $lat,
                'lon' => $lon,
                'fecha' => $fecha,
                'velocidad' => $velocidad,
                'direccion' => $direccion,
                'head' => $head
            );
        }
    }
}
?>
<div id="mensajes" style="padding:5px;text-align:center;display:none"></div>
<table border="0" cellspacing="2" cellpadding="2" class="table-responsive dataTable" style="width:100%;margin-bottom: 10px;font-size: 14px;">
    <tbody>
        <tr>
            <td style="padding:5px;vertical-align:top;width:15%;max-width:15%">
                <div class="form-group">
                    <label>Vehículos</label>
                    <?php
                    echo Select2::widget([
                        'name' => 'vehiculos',
                        'data' => Yii::$app->engine->getPatentesArrayLista(),
                        'size' => Select2::SMALL,
                        'options' => [
                            'placeholder' => 'seleccione patentes..',
                            'multiple' => true,
                            'class' => 'form-control',
                            'id' => 'vehiculos',
                            'style' => 'font-size:12px;width:100%'
                        ],
                        'pluginOptions' => [
                            'closeOnSelect' => true,
                            'maximumSelectionLength' => 10,
                            'allowClear' => false,
                            'theme' => 'classic',
                        ],
                    ]);
                    ?>
                </div>
            </td>
            <td style="padding:5px;text-align: center;vertical-align:middle;width:15%;max-width:15%">
                <button type="button" onclick="filtrar();" class="btn btn-danger btn-lg">Filtrar Datos</button>
            </td>
        </tr>
    </tbody>
</table>
<script type="text/javascript">
    var dataVehiculos = <?php echo json_encode($dataPatentesFinal); ?>
</script>

<div class="table-responsive" id="mosaico" style=""></div>
<script type="text/javascript">
    
    
    function filtrar() {
        $("#mensajes").html('<i style="color:red;vertical-align:middle" class="fas fa-cog fa-spin fa-2x"></i><span style="color:red;font-size:14px;margin-left:10px;margin-top:5px;">Buscando información por favor espere...</span>');
        $("#mensajes").show("slow");
        var vehiculos = $("#vehiculos").val();
        var url = "<?= Url::to(['reportes/mosaico'], true); ?>";
        window.location.href = url + "?vehiculos=" + vehiculos;
    }


    function creaMosaico() {
        var m = $("#mosaico");
        var cantidadVehiculos = dataVehiculos.length;
        var cantidad_filas = cantidadVehiculos / 3
        // console.log(cantidadVehiculos + " filas = " + cantidad_filas);
        var header = '<table border="0" cellspacing="2" cellpadding="2" style="" align="left"><tbody>';
        var footer = '</tbody></table>';
        var contadorFilas = 0;
        var contadorVehiculos = 0;
        var lineas = '';
        $.each(dataVehiculos, function (index) {
            var vehiculo = this;
            var id_patente = vehiculo.patente_id;
            contadorVehiculos++;
            if (contadorFilas <= cantidad_filas) {
                if (contadorVehiculos == 1) {
                    lineas = lineas + '<tr>';
                }
                if (contadorVehiculos <= 3) {
                    var mapa = '<div id="mapadiv_' + id_patente + '"  class="mapa google-maps"></div>';
                    if (vehiculo.evento != "") {
                        var eventoDiv = '<div id="evento_' + id_patente + '"><span class="badge badge-default badge-xsm" style="">' + vehiculo.evento + '</span></div>';
                    } else {
                        var eventoDiv = '<div id="evento_' + id_patente + '"></div>';
                    }
                    var forzar = '<span onclick="enviaRefresh(' + id_patente + ');" title="enviar peticion de registro" style="margin-left:35px;cursor:pointer;color:lightgreen" id="forzar_' + id_patente + '"><i class="fa fa-sync-alt"></i></span>';
                    var titulo = '<div class="titulomapa">' + vehiculo.muestra + ' - <span style="font-weight:bold;" id="fechamapa_' + id_patente + '">' + vehiculo.fecha + '</span>'+forzar + eventoDiv + vehiculo.direccion + '</div>';
                    lineas = lineas + '<td class="mapatd">' + titulo + mapa + '</td>';
                }
                if (contadorVehiculos == 3) {
                    lineas = lineas + '</tr>';
                    contadorFilas++;
                    contadorVehiculos = 0;
                }
            }
        });
        var mosaico = header + lineas + footer;
        $(m).html(mosaico);
    }

<?php
if (isset($_GET['vehiculos']) == true && $_GET['vehiculos'] != "") {
    echo '$("#vehiculos").val([' . $_GET['vehiculos'] . ']);';
}
?>

    $("#patente_div, #header_listado").remove();

    var mapa = null;
    var pintarMapaFlag = 1;
    var mapasArray = [];
    var markersArray = [];
    var actualizo = 1;
    var vehiculosDataUpdate = [];

    $(document).ready(function () {
        // console.log(dataVehiculos);
        creaMosaico();
        setInterval(function () {
            if (pintarMapaFlag == 1) {
                pintarMapaFlag = 0;
                pintarMapas();
            }
        }, 2000);

        setInterval(function () {
            if (actualizo == 1) {
                loadDataRefresh();
            }
        }, 30000);
        
        setInterval(function () {
            if (procesaPendientes == 1) {
                // console.log("procesados = "+procesados.length + " ---- pendientes = "+cuentaPendientes());
                procesaCidPendientes();
            }
        },5000);

    });

    function getIconoMovil(v) {
        // logica iconos y colores 
        var color = 0;
        var rotacion = v.device.latest.loc.head;
        var velocidad = parseInt(v.device.latest.loc.mph * 1.6);
        if (v.device.latest.vcounters.vehicle_dev_idle != undefined) {
            var segundos = v.device.latest.vcounters.vehicle_dev_idle;
            var tiempo_detenido_minutos = (segundos / 60);
            // verde
            if (tiempo_detenido_minutos > 0 && tiempo_detenido_minutos <= 5 && velocidad < 5) {
                color = 1;
            }
            // roja
            if (tiempo_detenido_minutos > 5 && tiempo_detenido_minutos <= 25 && velocidad < 5) {
                color = 2;
            }
            // stop rojo
            if (tiempo_detenido_minutos > 25 && tiempo_detenido_minutos <= 2900 && velocidad < 5) {
                color = 4;
            }
            // stop negro 
            if (tiempo_detenido_minutos > 2900 && velocidad < 5) {
                color = 5;
            }
        } else {
            color = 0;
        }
        var fillColor = ["green", "yellow", "red", "white"];
        var shape = {
            gray: 'm 15.832471,-142.36175 c 0,0 121.076359,-2.15352 114.751919,317.15236 -83.675559,-59.30589 -133.675559,-59.30589 -229.503849,0 -4.171711,-319.30588 115.82829,-319.30588 114.75193,-317.15236 z',
            blue: 'm -35.860415,-201.21297 c 0,0 76.278938,198.1153079 110.756059,306.10857 -95.39889,-79.999013 -125.957928,-58.687449 -221.512124,0 z'
        };
        var url = "<?= Yii::getAlias('@web'); ?>/images/stop_1.png";
        var url2 = "<?= Yii::getAlias('@web'); ?>/images/stop_3.png";

        // stop
        if (color == 4) {
            var image = {
                url: url,
                size: new google.maps.Size(32, 32),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 32),
                labelOrigin: new google.maps.Point(-15, -15)
            };
        }
        if (color == 5) {
            var image = {
                url: url2,
                size: new google.maps.Size(32, 32),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 32),
                labelOrigin: new google.maps.Point(-15, -15)
            };
        }

        if (color < 4) {
            var image = {
                labelOrigin: new google.maps.Point(-15, -15),
                path: shape["blue"], fillColor: fillColor[color],
                fillOpacity: 1,
                scale: 0.1, strokeColor: "black",
                strokeWeight: 1,
                rotation: parseInt(rotacion)};
        }
        return image;
    }

    function convertDate(epoch) {
        var fecha = new Date(epoch * 1000);
        var mes = fecha.getMonth() + 1;
        var minutos = fecha.getMinutes() < 10 ? '0' + fecha.getMinutes() : fecha.getMinutes();
        var segundos = fecha.getSeconds() < 10 ? '0' + fecha.getSeconds() : fecha.getSeconds();
        return fecha.getDate() + "/" + mes + "/" + fecha.getFullYear() + " " + fecha.getHours() + ":" + minutos + ":" + segundos;
    }
    
    function getIOs(v){
        var io_pwr = 'circulo_gris';
        var io_pwr_title = 'estado POWER ON';
        
        var io_ign = 'circulo_gris';
        var io_ign_title = 'estado IGNICION';
        
        var io_out1 = 'circulo_gris';
        var io_out1_title = 'CORTA CORRIENTE';
        
        var io_out2 = 'circulo_gris';
        var io_out2_title = 'PARADA SEGURA';
        
        var io_in1 = 'circulo_gris';
        var io_in1_title = 'PUERTA COPILOTO';
        
        var io_in2 = 'circulo_gris';
        var io_in2_title = 'BOTON PANICO';
        
        var io_in3 = 'circulo_gris';
        var io_in3_title = 'PUERTA PILOTO';
        
        if (v.device.latest.ios.io_pwr!=undefined){
           io_pwr = v.device.latest.ios.io_pwr == true ? 'circulo_verde' : 'circulo_rojo';
           io_pwr_title = v.device.latest.ios.io_pwr == true ? 'POWER ON' : 'POWER OFF';
        }
        if (v.device.latest.ios.io_ign!=undefined){
           io_ign = v.device.latest.ios.io_ign == true ? 'circulo_verde' : 'circulo_rojo';
           io_ign_title = v.device.latest.ios.io_ign == true ? 'IGNICION ON' : 'IGNICION OFF';
        }
        if (v.device.latest.ios.io_out1!=undefined){
           io_out1 = v.device.latest.ios.io_out1 == true ? 'circulo_rojo' : 'circulo_verde';
           io_out1_title = v.device.latest.ios.io_out1 == true ? 'CORTA CORRIENTE ACTIVO' : 'CORTA CORRIENTE INACTIVO';
        }
        if (v.device.latest.ios.io_out2!=undefined){
           io_out2 = v.device.latest.ios.io_out2 == true ? 'circulo_rojo' : 'circulo_verde';
           io_out2_title = v.device.latest.ios.io_out2 == true ? 'PARADA SEGURA ACTIVA' : 'PARADA SEGURA INACTIVA';
        }
        
        if (v.device.latest.ios.io_in1!=undefined){
           io_in1 = v.device.latest.ios.io_in1 == true ? 'circulo_rojo' : 'circulo_verde';
           io_in1_title = v.device.latest.ios.io_in1 == true ? 'PUERTA COPILOTO ABIERTA' : 'PUERTA COPILOTO CERRADA';
        }
        
        if (v.device.latest.ios.io_in2!=undefined){
           io_in2 = v.device.latest.ios.io_in2 == true ? 'circulo_rojo' : 'circulo_verde';
           io_in2_title = v.device.latest.ios.io_in2 == true ? 'BOTON PANICO ACTIVO' : 'BOTON PANICO INACTIVO';
        }
        
        if (v.device.latest.ios.io_in3!=undefined){
           io_in3 = v.device.latest.ios.io_in3 == true ? 'circulo_rojo' : 'circulo_verde';
           io_in3_title = v.device.latest.ios.io_in3 == true ? 'PUERTA PILOTO ABIERTA' : 'PUERTA PILOTO CERRADA';
        }
        var res = '<table border="0" style="font-size:10px" class="">' +
                '<tr><td style="text-align:left;padding:3px;">Power</td><td style="text-align:center"><div title="'+io_pwr_title+'" class="'+io_pwr+'">&nbsp;</div></td></tr>'+
                '<tr><td style="text-align:left;padding:3px;">Ignicion</td><td style="text-align:center"><div title="'+io_ign_title+'"  class="'+io_ign+'">&nbsp;</div></td></tr>'+
                '<tr><td style="text-align:left;padding:3px;">Corta Corriente</td><td style="text-align:center"><div title="'+io_out1_title+'"  class="'+io_out1+'">&nbsp;</div></td></tr>'+
                '<tr><td style="text-align:left;padding:3px;">Parada Segura</td><td style="text-align:center"><div title="'+io_out2_title+'"  class="'+io_out2+'">&nbsp;</div></td></tr>'+
                '<tr><td style="text-align:left;padding:3px;">Puerta Copiloto</td><td style="text-align:center"><div title="'+io_in1_title+'"  class="'+io_in1+'">&nbsp;</div></td></tr>'+
                '<tr><td style="text-align:left;padding:3px;">Boton Panico</td><td style="text-align:center"><div title="'+io_in2_title+'"  class="'+io_in2+'">&nbsp;</div></td></tr>'+
                '<tr><td style="text-align:left;padding:3px;">Puerta Piloto</td><td style="text-align:center"><div title="'+io_in3_title+'"  class="'+io_in3+'">&nbsp;</div></td></tr>'+
                '</table>'; 
        return res;
    } 

    function getDireccion(mapaid, vehiculoData, infow, mark, event) {
        var v = vehiculoData;
        //var ios = v.device.latest.ios;
        var datos = document.createElement('div');
        datos.style.width = '120px';
        datos.style.height = '130px';
        var close = document.createElement('div');
        close.align = "center";
        var closeLink = document.createElement('a');
        closeLink.innerHTML = "<span class='btn btn-danger btn-xsm'><b>Cerrar</b></span>";
        closeLink.href = "#";
        closeLink.onclick = function () {
            infow.close();
        };
        var ios = getIOs(v);
        close.appendChild(closeLink);
        var contentString = document.createElement('div');
        contentString.align = "left";
        contentString.innerHTML = '' + ios;
        // datos.appendChild(close);
        datos.appendChild(contentString);
        infow.setContent(datos);
        if (event != undefined) {
            infow.setPosition(event.LatLng);
        }
        infow.open(mapaid, mark);
        return true;
    }

    function loadMarkets() {
        markersArray = [];
        if (vehiculosDataUpdate.length > 0) {
            $.each(vehiculosDataUpdate, function (index, v) {
                var dataRemota = v;
                $.each(dataVehiculos, function (index) {
                    if (this.patente_id == dataRemota.id) {
                        vehiculosArray[dataRemota.id] = dataRemota;
                        var iconoMovil = getIconoMovil(dataRemota);
                        var marker = new google.maps.Marker({
                            position: {lat: parseFloat(dataRemota.device.latest.loc.lat), lng: parseFloat(dataRemota.device.latest.loc.lon)},
                            draggable: false,
                            map: mapasArray[this.patente_id],
                            icon: iconoMovil,
                            labelClass: 'etiquetas',
                        });
                        markersArray.push(marker);
                        mapasArray[this.patente_id].setCenter(new google.maps.LatLng(dataRemota.device.latest.loc.lat, dataRemota.device.latest.loc.lon));
                        var infowindow = new google.maps.InfoWindow;
                        marker.addListener('click', function (event) {
                            if (infowindow) {
                                infowindow.close();
                            }
                            getDireccion(mapasArray[this.patente_id], v, infowindow, marker, event);
                        });
                        google.maps.event.addListener(mapasArray[this.patente_id], "click", function (event) {
                            infowindow.close();
                        });
                        eventoName = '';
                        nuevaFecha = '';
                        if (dataRemota.device.latest.loc.evtime != undefined) {
                            var tiempo = dataRemota.device.latest.loc.evtime;
                            if (tiempo != "") {
                                nuevaFecha = convertDate(tiempo);
                            }
                        }
                        if (dataRemota.device.latest.loc.code != undefined) {
                           var eventoNameValue = dataRemota.device.latest.loc.code;
                           var eventoLabel = getEventoName(eventoNameValue);
                           if (eventoLabel!=""){
                                eventoName = '<span class="badge badge-default badge-xsm" style="">' + eventoLabel + '</span>';
                           }
                        }
                        $("#fechamapa_" + this.patente_id).html(nuevaFecha);
                        // nuevo evento 
                        $("#evento_"+this.patente_id).html(eventoName);
                        
                        // console.log(dataRemota);
                    }
                });
            });
        }
    }



    function loadDataRefresh() {
        // elimino todos los marcadores previos 
        $("#mensajes").html('<i style="color:red;vertical-align:middle" class="fas fa-cog fa-spin fa-2x"></i><span style="color:red;font-size:14px;margin-left:10px;margin-top:5px;">Actualizando posicion de los vehiculos, por favor espere...</span>');
        $("#mensajes").show("slow");
        var marcadoresOld = markersArray;
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/get-ulpos'; ?>",
            dataType: "html",
            async: true,
            success: function (response) {
                vehiculosDataUpdate = JSON.parse(response);
                loadMarkets();

                // borro los markers viejos
                $.each(marcadoresOld, function (e) {
                    if (this != undefined && this.setMap != undefined) {
                        this.setMap(null);
                    }
                });
                $("#mensajes").html('');
                $("#mensajes").hide("slow");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#mensajes").html('');
                $("#mensajes").hide("slow");
            }
        });
        return true;
    }

    function muestraMapa(index, id, lat, lon, v) {
        var mapOptions = {
            center: new google.maps.LatLng(lat, lon),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: 12
        };
        mapasArray[id] = new google.maps.Map(document.getElementById("mapadiv_" + id), mapOptions);
    }
    
    function getEventoName(evento_id){
        var res = '';
        var codigosEventos = [30,31,44,45,22,23,3,4,6,7,53,58];
        if (eventosArray[0]!=undefined){
            $.each(eventosArray[0], function(index){
                var eventoData = this;
                if (evento_id == index && codigosEventos.indexOf(evento_id)!=-1){
                    res = eventoData;
                }
            });
        }
        return res;
    }

    function pintarMapas() {
        $.each(dataVehiculos, function (index) {
            // mapasArray
            var contador = index + 1;
            $("#mapadiv_" + this.patente_id).html(index + "----" + this.muestra);
            muestraMapa(contador, this.patente_id, this.lat, this.lon, this);
        });
        loadDataRefresh();
    }

    var eventosArray = [<?= json_encode($eventosArray); ?>];
    
    
    var vehiculosArray = [];
    var pendientes = [];
    var procesados = [];
    var procesaPendientes = 0;

    
    // **************** comandos de forzar registro
    function quitarDePendientes(id){
        var newPendientes = [];
        if (pendientes.length > 0){
            $.each(pendientes, function(key,value){
                    if (key != id){
                        newPendientes[key] = value;
                    }
            });
            pendientes = newPendientes;
        }
    }
    
    function cuentaPendientes(){
        var res = 0;
        if (pendientes.length > 0){
            $.each(pendientes, function(key,value){
                    if (value != "" && value > 0 && value!=undefined){
                        res++;
                    }
            });
        }
        return res;
    }
    
    function setEspera(estado,id){
        var iconoVerde = '<i style="color:lightgreen" class="fa fa-sync-alt"></i>';
        var iconoAmarillo = '<i style="color:yellow" class="fa fa-sync-alt fa-spin"></i>';
        if (estado == 2){
            $("#forzar_"+id).html(iconoAmarillo);
        }
        if (estado == 1){
            $("#forzar_"+id).html(iconoVerde);
            // pongo ese id en procesados
            procesados.push(id); 
        }
        // es un vehiculo que no deja de estar en amarillo
        if (estado == 3){
            $("#forzar_"+id).html(iconoVerde);
            quitarDePendientes(id);
        }
    }
    
    function estaProcesado(id){
        var res = false;
        $.each(procesados, function(indice, valor){
                if (id == valor){
                    res = true; 
                    // console.log("----ID = " + id + "--PROCESADO TRUE");
                }
        });
        return res;
    }
    
    function estaPendiente(id){
        var res = false;
        if (pendientes.length > 0){
            $.each(pendientes, function(key,value){
                    if (key == id && value > 0 && value!=undefined && value!=""){
                        res = true;
                    }
            });
        }
        return res
    }
    
    function procesaCidPendientes(){
        if (pendientes.length > 0){
            $.each(pendientes, function(key,value){
                    if (value!=undefined){
                        // console.log(key + " cid = " + value);
                        // veo si ya fue procesado para no esperar mas response
                        if (estaProcesado(key) == false){
                            esperaResponse(key);
                        }
                    }
            });
        }
        if (procesados.length == cuentaPendientes()){
            procesaPendientes = 0;
            procesados = [];
            pendientes = [];
            // console.log("Ya no hay mas pendientes");
        }
    }
    
    
    function enviaRefresh(id) {
        var dataVehiculo = vehiculosArray[id];
        if (estaPendiente(id)==true){
            setEspera(3,id); // lo mando a sacar de los pendientes y ponemos icono verde
            // console.log("ya esta pendiente = "+id);
            return false;
        }
        var comando = '>SXACT002<';
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/envia-comando'; ?>",
            dataType: "html",
            data: {id: id, comando: comando},
            success: function (response) {
                setEspera(2,id);
                var responseJson = JSON.parse(response);
                // console.log("---- ENVIOOOO --- "+comando);
                // console.log(responseJson);
                if (responseJson.msg != undefined) {
                    // console.log(id + " -- mensaje = "+responseJson.msg);
                    // alert("Comando enviado - MSG [" + responseJson.msg + "]");
                }
                if (responseJson.cid != undefined) {
                    // console.log("Se envia a procesar ID = "+id + "-- CID = "+responseJson.cid);
                    pendientes[id] = responseJson.cid;
                    procesaPendientes = 1;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
    }    
    
    function esperaResponse(id) {
        var cid = pendientes[id]!=undefined ? pendientes[id] : "";
        if (cid==""){return false;}
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->request->baseUrl . '/site/espera-response'; ?>",
            dataType: "html",
            data: {id: id, cid: cid},
            success: function (response) {
                var responseJson = JSON.parse(response);
                // console.log("----------- RESPONSE ----- ");
                // console.log(responseJson);
                if (responseJson.message!=undefined){
                    // console.log(id + "--  mensaje = " + responseJson.message);
                    if (responseJson.message == 'command not found'){
                        return false;
                    }
                    if (responseJson.message != 'command not found'){
                        // RXACT002
                        setEspera(1,id);
                        loadDataRefresh();
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Problemas enviado datos : \n" + xhr.responseText);
            }
        });
    }



</script>
<div style="margin-top:50px;">&nbsp;</div>
<style>
    .summary {
        font-size: 14px;
        text-align: right;
    }

    .pagination {
        font-size: 14px;
    }

    #registros-grid-container thead{
        background-color: lightgray;
    }

    tbody .select2-container .select2-selection {
        height: 100px;
        overflow-y: scroll;
        overflow-x: hidden;
    } 

    .s2-togall-button{
        display:none;
    }

    .bootstrap-timepicker .inline-addon {
        display:none!important;
    }

    .kv-grid-loading {
        position: relative;
        overflow: hidden;
        max-height: 100%;
    }

    .kv-grid-loading .kv-loader {
        position: absolute;
        width: 100px !important;
        height: 100px !important;
        top: 20px!important;
        left: 50% !important;
    }

    .mapa{
        width:350px;
        max-width: 350px;
        height:350px;
    }

    .mapatd {
        padding:15px;
    }

    .titulomapa{
        width:350px;
        max-width: 350px;
        font-size:11px;
        padding:3px;
        height:68px;
        background-color: #006E50;
        color:white;
        vertical-align: top;
    }
    
    .circulo_rojo {
        border-radius: 50%;
        width: 15px;
        height: 15px;
        margin-left: 30%;
        background: red;
        border: 1px solid black;
    }

    .circulo_verde {
        border-radius: 50%;
        width: 15px;
        height: 15px;
        margin-left: 30%;
        background: green;
        border: 1px solid black;
    }

    .circulo_gris {
        border-radius: 50%;
        width: 15px;
        height: 15px;
        margin-left: 30%;
        background: lightgray;
        border: 1px solid black;
    }

</style>