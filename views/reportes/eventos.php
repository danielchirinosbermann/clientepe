<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\export\ExportMenu;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;

$this->title = 'Reporte Eventos';
$bread1 = 'Resumen Eventos';
$bread2 = $this->title;
$bread3 = '';
$this->params['activeLink'] = "reporte-eventos";

ini_set('max_execution_time', '99600');
ini_set('max_input_time', '-1');
ini_set('memory_limit', '-1');
set_time_limit(9600);
?>

<!-- Page-Title -->
<div class="row" style="margin-top: 20px;">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h2 class="page-title" style="font-size: 16px;"><?= $this->title ?></h2>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php
    /*$imei = '356612022379244';
    $res = Yii::$app->engine->getDatosIO($imei);  
    echo '<pre>';
    var_dump($res);
    echo '</pre>';*/

?>



<?php

// Yii::$app->engine->getEventosArray();
// Yii::$app->engine->getPatentesArray();

function dataProvider() {
    $vehiculos = null;
    // $filtro1 = '10,53,58,23,22,44,45';
    $eventos = '22,23,53,58,03,04,10,83,86,44,45';
    $grupos = null;
    if (isset($_GET['vehiculos']) == true && $_GET['vehiculos'] != "") {
        $vehiculos = $_GET['vehiculos'];
    }
    if (isset($_GET['eventos']) == true && $_GET['eventos'] != "") {
        $eventos = $_GET['eventos'];
    }
    $fechaDesde = date('Y-m-d\T00:00:00');
    $fechaHasta = date('Y-m-d\T23:59:59');    
    $dataArray = Yii::$app->engine->getDatosEventos($vehiculos, $eventos, $fechaDesde, $fechaHasta);
    $data = [];
    $contador = 0;
    $UTC = new DateTimeZone("UTC");
    if (is_array($dataArray) == true && count($dataArray) > 0) {
        $tempVehiculo = '';
        $contadorLimite = 0;
        foreach ($dataArray as $dataRecord) {
            $contador++;
            $eventoName = $dataRecord->code > 0 ? Yii::$app->engine->getEventoName($dataRecord->code) : "";
            $vehiculoName = $dataRecord->vid > 0 ? Yii::$app->engine->getPatenteName($dataRecord->vid) : "";
            // $mapaLink = '<a title="muestra posición en google maps" pjax="0" href="https://www.google.com/maps/search/?api=1&query=' . $dataRecord->lat . ',' . $dataRecord->lon . '" target="_blank"><i class="fa fa-map-marked fa-2x"></i></a>';
            $fecha = date('d/m/Y H:i:s', strtotime($dataRecord->event_time . ' UTC'));
            $contadorLimite++;
            if ($tempVehiculo!="" && $tempVehiculo!=$vehiculoName){
                $data[] = [
                    'id' => $contador,
                    'vehiculo' => $vehiculoName,
                    'velocidad' => $dataRecord->speed,
                    'evento' => $eventoName,
                    'fecha' => $fecha
                    //'mapa' => $mapaLink
                ];
                $contadorLimite = 0;
            }
            if ($tempVehiculo == ""){
                $tempVehiculo = $vehiculoName;
            }
            if ($tempVehiculo == $vehiculoName && $contadorLimite <= 10){
                $data[] = [
                    'id' => $contador,
                    'vehiculo' => $vehiculoName,
                    'velocidad' => $dataRecord->speed,
                    'evento' => $eventoName,
                    'fecha' => $fecha
                    //'mapa' => $mapaLink
                ];
            }
            $tempVehiculo = $vehiculoName;
            
        }
    }

    return new ArrayDataProvider([
        'allModels' => $data,
        'pagination' => [
            'pageSize' => 12000,
        ],
    ]);
}
?>

<?php
$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    'vehiculo',
    'evento',
    [
        'label' => 'Fecha',
        'attribute' => 'fecha',
        'format' => 'html',
        'options' => ['style' => 'width:15%;max-width:15%;'],
    ],
];

$datos = dataProvider();
$totalDatos = $datos->getTotalCount();

$descargarLink = ExportMenu::widget([
            'dataProvider' => $datos,
            'columns' => $gridColumns,
            'asDropdown' => false,
            'template' => '{menu}',
            'showConfirmAlert' => false,
            'showColumnSelector' => false,
            'filename' => 'registrosExportadosOnline',
            'fontAwesome' => false,
            'exportConfig' => [
                'Html' => false,
                'Csv' => false,
                'Txt' => false,
                'Pdf' => false,
                'Xls' => false,
                'Xlsx' => [
                    'label' => 'Excel'
                ],
            ],
        ]);
?>
<?php
if ($totalDatos > 0) {
    echo '<div class="col-md-6 alert alert alert-danger" style="font-size:14px;"><label>Descargar Datos</label>' . $descargarLink . '</div>';
}
?>
<div id="mensajes" style="padding:5px;text-align:center;display:none"></div>
<table border="0" cellspacing="2" cellpadding="2" class="table-responsive dataTable" style="width:100%;margin-bottom: 10px;font-size: 14px;">
    <tbody>
        <tr>
            <td style="padding:5px;vertical-align:top;width:35%;max-width:35%">
                <div class="form-group">
                    <label>Vehículos <span style="margin-left:20px;font-size: 14px;" class="btn btn-success btn-sm" onclick="seleccionaTodos();">Seleccionar Todos</span>&nbsp;&nbsp;<span style="margin-left:20px;font-size: 14px;" class="btn btn-danger btn-sm" onclick="$('#vehiculos > option').prop('selected',false);$('#vehiculos').trigger('change');">Quitar Todos</span></label>
                    <?php
                    echo Select2::widget([
                        'name' => 'vehiculos',
                        'data' => Yii::$app->engine->getPatentesArrayLista(),
                        'size' => Select2::SMALL,
                        'options' => [
                            'placeholder' => 'seleccione patentes..',
                            'multiple' => true,
                            'class' => 'form-control',
                            'id' => 'vehiculos',
                            'style' => 'font-size:12px;width:100%'
                        ],
                        'pluginOptions' => [
                            'closeOnSelect' => true,
                            'maximumSelectionLength' => 200,
                            'allowClear' => false,
                            'theme' => 'classic',
                        ],
                    ]);
                    ?>
                </div>
            </td>
            <td style="padding:5px;vertical-align:top;width:35%;max-width:35%">
                <div class="form-group">
                    <label>Eventos &nbsp;&nbsp;<span style="margin-left:20px;font-size: 14px;" class="btn btn-success btn-sm" onclick="$('#eventos').val([10,53,58,23,22,44,45]);$('#eventos').trigger('change');">Filtro 1</span>&nbsp;&nbsp;<span style="margin-left:20px;font-size: 14px;" class="btn btn-danger btn-sm" onclick="$('#eventos > option').prop('selected',false);$('#eventos').trigger('change');">Quitar Todos</span></label>
                    <?php
                    echo Select2::widget([
                        'name' => 'eventos',
                        'data' => Yii::$app->engine->getEventosArrayListaEventos(),
                        'size' => Select2::SMALL,
                        'options' => [
                            'placeholder' => 'seleccione eventos..',
                            'multiple' => true,
                            'id' => 'eventos',
                            'class' => 'form-control',
                            'style' => 'font-size:12px;width:100%;'
                        ],
                        'pluginOptions' => [
                            'allowClear' => false,
                            'theme' => 'classic',
                        ],
                    ]);
                    ?>

                </div>
            </td>
            <td style="padding:5px;text-align: center;vertical-align:middle;width:150px;max-width:150px;">
                <button type="button" onclick="setConteo();" class="btn btn-success btn-lg">Detiene / Continua Recarga de Datos</button>
                <div style="text-align:center" id="conteo">
                    <legend style="color:blue"><i class="fa fa-cog fa-spin fa-1x"></i> &nbsp;&nbsp;Recarga de Datos en : </legend>
                    <h2 id="contadorSegundos" class="" style="font-size:18px;font-weight:bold;margin:5px;color:red">0</h2>
                </div>
                <!-- <button type="button" onclick="filtrar();" class="btn btn-danger btn-lg">Filtrar Datos</button> -->                
            </td>
        </tr>
    </tbody>
</table>

<div class="card-box table-responsive">  
    <?php Pjax::begin(['timeout' => false]); ?>
    <?=
    GridView::widget([
        'id' => 'registros',
        'dataProvider' => $datos,
        'tableOptions' => ['class' => 'table', 'style' => 'width:100%;font-size:14px;'],
        'responsive' => true,
        'responsiveWrap' => true,
        'bordered' => true,
        'striped' => false,
        'bootstrap' => true,
        'pjaxSettings'=>[
            'loadingCssClass'=>true,
        ],
        /* 'floatHeader' => true, */
        /* 'floatHeaderOptions' => ['top' => '50'], */
        'hover' => true,
        'pjax' => true,
        'columns' => [
            [
                'label' => 'Patente',
                'attribute' => 'vehiculo',
                'format' => 'html',
                'options' => ['style' => 'width:15%;max-width:15%;'],
            ],
            [
                'label' => 'Evento',
                'attribute' => 'evento',
                'format' => 'html',
                'options' => ['style' => 'width:15%;max-width:15%;'],
            ],
            [
                'label' => 'Fecha',
                'attribute' => 'fecha',
                'format' => 'html',
                'options' => ['style' => 'width:15%;max-width:15%;'],
            ],
            /*[
                'label' => 'Mapa',
                'attribute' => 'mapa',
                'format' => 'raw',
                'options' => ['style' => 'width:15%;max-width:15%;'],
            ],*/
        ],
    ]);
    ?>
<?php Pjax::end(); ?>
</div>

<script type="text/javascript">
    
    var contadorSegundos = 0;
    var puedoContar = 1;
    var iniciaConteo = 1;
    
    function setConteo(){
        if (iniciaConteo == 1){
            iniciaConteo = 0;
            $("#conteo").hide("slow");
            contadorSegundos = 0;
            puedoContar = 0;
        } else {
            iniciaConteo = 1;
            $("#conteo").show("slow");
            puedoContar = 1;
        }
    }
    
    $(document).ready(function () {
        setInterval(function () {
                if (iniciaConteo == 1){
                    if (puedoContar == 1){
                        contadorSegundos++;
                        var segundos = 30;
                        $("#contadorSegundos").html(segundos - contadorSegundos);
                        if (contadorSegundos == segundos){
                            filtrar();
                            contadorSegundos = 0;
                            puedoContar = 0;
                        }
                    }
                }
        }, 1000);

    });

    function filtrar() {
        var fechadesde = $("#fecha_desde").val();
        var fechahasta = $("#fecha_hasta").val();
        var hora_desde = $("#hora_desde").val();
        var hora_hasta = $("#hora_hasta").val();
        if (fechadesde == "" || fechahasta == "") {
            alert("Complete la seleccion de fechas antes de filtrar la informacion\n\nSi desea consultar un dia, seleccione la misma fecha en ambos campos.");
            return false;
        }
        var desde = hora_desde != "" ? fechadesde + ' ' + hora_desde : fechadesde;
        var hasta = hora_hasta != "" ? fechahasta + ' ' + hora_hasta : fechahasta;
        $("#mensajes").html('<i style="color:red;vertical-align:middle" class="fas fa-cog fa-spin fa-2x"></i><span style="color:red;font-size:14px;margin-left:10px;margin-top:5px;">Buscando información por favor espere...</span>');
        $("#mensajes").show("slow");
        var vehiculos = $("#vehiculos").val();
        var eventos = $("#eventos").val();
        var url = "<?= Url::to(['reportes/eventos'], true); ?>";
        window.location.href = url + "?vehiculos=" + vehiculos + "&eventos=" + eventos;
    }

    function setVehiculos(grupo) {
        if (grupo > 0) {
            $("#mensajes").html('<i style="color:red;vertical-align:middle" class="fas fa-cog fa-spin fa-2x"></i><span style="color:red;font-size:14px;margin-left:10px;margin-top:5px;">Buscando vehiculos del grupo seleccionado...</span>');
            $("#mensajes").show("slow");
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::$app->request->baseUrl . '/site/vehiculos-grupo'; ?>",
                dataType: "html",
                async: true,
                data: {grupo: grupo},
                success: function (response) {

                    var b = response.split(',').map(function (item) {
                        return parseInt(item);
                    });
                    $("#vehiculos").val(b);
                    $("#vehiculos").trigger('change');
                    $("#mensajes").html('');
                    $("#mensajes").hide("slow");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Problemas enviado datos : \n" + xhr.responseText);
                }
            });

        }
    }

<?php
if (isset($_GET['vehiculos']) == true && $_GET['vehiculos'] != "") {
    echo '$("#vehiculos").val([' . $_GET['vehiculos'] . ']);';
}
if (isset($_GET['eventos']) == true && $_GET['eventos'] != "") {
    echo '$("#eventos").val([' . $_GET['eventos'] . ']);';
}

?>

    $("#patente_div, #header_listado, .topbar, .side-menu").hide();
    
    function seleccionaTodos(){
        var contador = 0;
        $('#vehiculos > option').each(function(){
            contador++;
            if (contador <= 20){
                $(this).prop('selected','selected');$('#vehiculos').trigger('change');
            }
        });
                
    }
    

</script>

<style>
    .summary {
        font-size: 14px;
        text-align: right;
    }

    .pagination {
        font-size: 14px;
    }

    #registros-grid-container thead{
        background-color: lightgray;
    }

    tbody .select2-container .select2-selection {
        height: 100px;
        overflow-y: scroll;
        overflow-x: hidden;
    } 

    .s2-togall-button{
        display:none;
    }
    
    .bootstrap-timepicker .inline-addon {
        display:none!important;
    }

    .kv-grid-loading {
        position: relative;
        overflow: hidden;
        max-height: 100%;
    }
    
    .kv-grid-loading .kv-loader {
        position: absolute;
        width: 100px !important;
        height: 100px !important;
        top: 20px!important;
        left: 50% !important;
    }
    
    .registros {
        padding:1px!important;
    }

</style>