<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\export\ExportMenu;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;

$this->title = 'Reporte Historico';
$bread1 = 'Reporte Histórico';
$bread2 = $this->title;
$bread3 = '';
$this->params['activeLink'] = "reporte-historico";


ini_set('max_execution_time', '99600');
ini_set('max_input_time', '-1');
ini_set('memory_limit', '-1');
set_time_limit(9600);
?>

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h2 class="page-title" style="font-size: 16px;"><?= $this->title ?></h2>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php

// Yii::$app->engine->getEventosArray();
// Yii::$app->engine->getPatentesArray();

function dataProvider() {
    $vehiculos = null;
    $eventos = null;
    $grupos = null;
    if (isset($_GET['vehiculos']) == true && $_GET['vehiculos'] != "") {
        $vehiculos = $_GET['vehiculos'];
    }
    if (isset($_GET['eventos']) == true && $_GET['eventos'] != "") {
        $eventos = $_GET['eventos'];
    }
    $fechaDesde = '';
    if (isset($_GET['desde']) == true && $_GET['desde'] != "") {
        $dateArray = explode(' ', $_GET['desde']);
        $dateArrayDate = explode('/', $dateArray[0]);
        $fechaDesde = $dateArrayDate[2] . '-' . $dateArrayDate[1] . '-' . $dateArrayDate[0] . 'T' . $dateArray[1] . ':00';
        $fI = date("Y-m-d H:i:00", strtotime(str_replace("T"," ",$fechaDesde. "+4 hours")));
        $fI = str_replace(" ","T",$fI);
        $fechaDesde = $fI;
    }
    $fechaHasta = '';
    if (isset($_GET['hasta']) == true && $_GET['hasta'] != "") {
        $dateArray = explode(' ', $_GET['hasta']);
        $dateArrayDate = explode('/', $dateArray[0]);
        $fechaHasta = $dateArrayDate[2] . '-' . $dateArrayDate[1] . '-' . $dateArrayDate[0] . 'T' . $dateArray[1] . ':00';
        $fF = date("Y-m-d H:i:00", strtotime(str_replace("T"," ",$fechaHasta. "+4 hours")));
        $fF = str_replace(" ","T",$fF);
        $fechaHasta = $fF;
    }

    $dataArray = Yii::$app->engine->getDatosHistoricos($vehiculos, $eventos, $fechaDesde, $fechaHasta);
    $data = [];
    $contador = 0;
    $UTC = new DateTimeZone("UTC");
    if (is_array($dataArray) == true && count($dataArray) > 0) {
        foreach ($dataArray as $dataRecord) {
            $contador++;
            $eventoName = $dataRecord->code > 0 ? Yii::$app->engine->getEventoName($dataRecord->code) : "";
            $vehiculoName = $dataRecord->vid > 0 ? Yii::$app->engine->getPatenteName($dataRecord->vid) : "";
            $mapaLink = '<a title="muestra posición en google maps" pjax="0" href="https://www.google.com/maps/search/?api=1&query=' . $dataRecord->lat . ',' . $dataRecord->lon . '" target="_blank"><i class="fa fa-map-marked fa-2x"></i></a>';
            $fecha = date('d/m/Y H:i:s', strtotime($dataRecord->event_time . ' UTC'));
            $fechaSistema = date('d/m/Y H:i:s', strtotime($dataRecord->system_time . ' UTC'));
            $data[] = [
                'id' => $contador,
                'vehiculo' => $vehiculoName,
                'velocidad' => $dataRecord->speed,
                'evento' => $eventoName,
                'fecha' => $fecha,
                'fecha_sistema'=>$fechaSistema,
                'mapa' => $mapaLink
            ];
        }
    }

    return new ArrayDataProvider([
        'allModels' => $data,
        'pagination' => [
            'pageSize' => 200,
        ],
        'sort' => [
            'defaultOrder' => 'fecha desc',
            'attributes' => ['id', 'vehiculo', 'fecha','fecha_sistema','velocidad', 'evento'],
        ],
    ]);
}
?>

<?php
$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    'vehiculo',
    'evento',
    'velocidad',
    [
        'label' => 'Fecha',
        'attribute' => 'fecha',
        'format' => 'html',
        'options' => ['style' => 'width:15%;max-width:15%;'],
    ],
    [
        'label' => 'Fecha Sistema',
        'attribute' => 'fecha_sistema',
        'format' => 'html',
        'options' => ['style' => 'width:15%;max-width:15%;'],
    ],
];

$datos = dataProvider();
$totalDatos = $datos->getTotalCount();

$descargarLink = ExportMenu::widget([
            'dataProvider' => $datos,
            'columns' => $gridColumns,
            'asDropdown' => false,
            'template' => '{menu}',
            'showConfirmAlert' => false,
            'showColumnSelector' => false,
            'filename' => 'registrosExportadosOnline',
            'fontAwesome' => false,
            'exportConfig' => [
                'Html' => false,
                'Csv' => false,
                'Txt' => false,
                'Pdf' => false,
                'Xls' => false,
                'Xlsx' => [
                    'label' => 'Excel'
                ],
            ],
        ]);
?>
<?php
if ($totalDatos > 0) {
    echo '<div class="col-md-6 alert alert alert-danger" style="font-size:14px;"><label>Descargar Datos</label>' . $descargarLink . '</div>';
}
?>
<div id="mensajes" style="padding:5px;text-align:center;display:none"></div>
<table border="0" cellspacing="2" cellpadding="2" class="table-responsive dataTable" style="width:100%;margin-bottom: 10px;font-size: 14px;">
    <tbody>
        <tr>
            <td style="padding:5px;vertical-align:top;width:15%;max-width:15%">
                <div class="form-group">
                    <label>Vehículos</label>
                    <?php
                    echo Select2::widget([
                        'name' => 'vehiculos',
                        'data' => Yii::$app->engine->getPatentesArrayLista(),
                        'size' => Select2::SMALL,
                        'options' => [
                            'placeholder' => 'seleccione patentes..',
                            'multiple' => true,
                            'class' => 'form-control',
                            'id' => 'vehiculos',
                            'style' => 'font-size:12px;width:100%'
                        ],
                        'pluginOptions' => [
                            'closeOnSelect' => true,
                            'maximumSelectionLength' => 10,
                            'allowClear' => false,
                            'theme' => 'classic',
                        ],
                    ]);
                    ?>
                </div>
            </td>
            <td style="padding:5px;vertical-align:top;width:15%;max-width:15%">
                <div class="form-group">
                    <label>Eventos</label>
                    <?php
                    echo Select2::widget([
                        'name' => 'eventos',
                        'data' => Yii::$app->engine->getEventosArrayLista(),
                        'size' => Select2::SMALL,
                        'options' => [
                            'placeholder' => 'seleccione eventos..',
                            'multiple' => true,
                            'id' => 'eventos',
                            'class' => 'form-control',
                            'style' => 'font-size:12px;width:100%;'
                        ],
                        'pluginOptions' => [
                            'allowClear' => false,
                            'theme' => 'classic',
                        ],
                    ]);
                    ?>

                </div>
            </td>
            <td style="padding:5px;vertical-align:top;width:15%;max-width:15%">
                <div class="form-group">
                    <?php
                    echo '<label class="control-label">Fecha Inicio</label>';
                    echo DatePicker::widget([
                        'name' => 'fecha_desde',
                        'removeButton' => false,
                        'type' => DatePicker::TYPE_INPUT,
                        'options' => [
                            'placeholder' => 'fecha inicio ...',
                            'class' => 'form-control datetimepicker datetimepicker-input',
                            'style' => ['font-size:14px;height:30px;font-weight:bold;width:100px;max-width:100px;'],
                            'id' => 'fecha_desde',
                        ],
                        'pluginEvents' => [
                            'changeDate' => 'function(e) {  validaFechas(); }',
                        ],
                        'pluginOptions' => [
                            'endDate' => 'new date()',
                            'convertFormat' => true,
                            'orientation' => 'bottom left',
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            'format' => 'dd/mm/yyyy',
                            'autoclose' => true,
                        ]
                    ]);
                    ?>
                    <?php
                    echo '<label class="control-label">Hora Inicio</label>';
                    echo TimePicker::widget([
                        'name' => 'hora_desde',
                        'size' => 'md',
                        'addonOptions' => [
                            'asButton' => false,
                            'buttonOptions' => ['style' => 'display:none']
                        ],
                        'options' => [
                            'placeholder' => 'hora inicio ...',
                            'class' => 'form-control',
                            'style' => ['font-size:14px;height:30px;font-weight:bold;width:100px;max-width:100px;'],
                            'id' => 'hora_desde',
                            'maxlength' => 5,
                        ],
                        'pluginOptions' => [
                            'showRightIcon' => false,
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => '00:00',
                            'template' => false
                        ]
                    ]);
                    ?>


                </div>
            </td>
            <td style="padding:5px;vertical-align:top;width:15%;max-width:15%">
                <div class="form-group">
                    <?php
                    echo '<label class="control-label">Fecha Fin</label>';
                    echo DatePicker::widget([
                        'name' => 'fecha_hasta',
                        'removeButton' => false,
                        'type' => DatePicker::TYPE_INPUT,
                        'options' => [
                            'placeholder' => 'fecha fin ...',
                            'class' => 'form-control datetimepicker datetimepicker-input',
                            'style' => ['font-size:14px;height:30px;font-weight:bold;width:100px;max-width:100px;'],
                            'id' => 'fecha_hasta',
                        ],
                        'pluginEvents' => [
                            'changeDate' => 'function(e) {  validaFechas(); }',
                        ],
                        'pluginOptions' => [
                            'endDate' => 'new date()',
                            'convertFormat' => true,
                            'orientation' => 'bottom left',
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            'format' => 'dd/mm/yyyy',
                            'autoclose' => true,
                        ]
                    ]);
                    ?>                  
                    <?php
                    echo '<label class="control-label">Hora Fin</label>';
                    echo TimePicker::widget([
                        'name' => 'hora_hasta',
                        'size' => 'md',
                        'addonOptions' => [
                            'asButton' => false,
                            'buttonOptions' => ['style' => 'display:none']
                        ],
                        'options' => [
                            'placeholder' => 'hora fin ...',
                            'class' => 'form-control',
                            'style' => ['font-size:14px;height:30px;font-weight:bold;width:100px;max-width:100px;'],
                            'id' => 'hora_hasta',
                            'maxlength' => 5,
                        ],
                        'pluginOptions' => [
                            'showRightIcon' => false,
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => '23:59',
                            'template' => false
                        ]
                    ]);
                    ?>


                </div>
            </td>
            <td style="padding:5px;text-align: center;vertical-align:middle;width:15%;max-width:15%">
                <button type="button" onclick="filtrar();" class="btn btn-danger btn-lg">Filtrar Datos</button>
            </td>
        </tr>
    </tbody>
</table>

<div class="card-box table-responsive">  
    <?php Pjax::begin(['timeout' => false]); ?>
    <?=
    GridView::widget([
        'id' => 'registros',
        'dataProvider' => $datos,
        'tableOptions' => ['class' => 'table', 'style' => 'width:100%;font-size:14px;'],
        'responsive' => true,
        'responsiveWrap' => true,
        'bordered' => true,
        'striped' => false,
        'bootstrap' => true,
        'pjaxSettings'=>[
            'loadingCssClass'=>true,
        ],
        /* 'floatHeader' => true, */
        /* 'floatHeaderOptions' => ['top' => '50'], */
        'hover' => true,
        'pjax' => true,
        'columns' => [
            'vehiculo',
            'evento',
            'velocidad',
            [
                'label' => 'Fecha Evento',
                'attribute' => 'fecha',
                'format' => 'html',
                'options' => ['style' => 'width:15%;max-width:15%;'],
            ],
            [
                'label' => 'Fecha Sistema',
                'attribute' => 'fecha_sistema',
                'format' => 'html',
                'options' => ['style' => 'width:15%;max-width:15%;'],
            ],
            [
                'label' => 'Mapa',
                'attribute' => 'mapa',
                'format' => 'raw',
                'options' => ['style' => 'width:15%;max-width:15%;'],
            ],
        ],
    ]);
    ?>
<?php Pjax::end(); ?>
</div>

<script type="text/javascript">

    function validaFechas()
    {
        var fechadesde = $("#fecha_desde").val();
        var fechahasta = $("#fecha_hasta").val();
        if (fechadesde == "" || fechahasta == "") {
            return false;
        }
        var desde = fechadesde.split("/");
        var hasta = fechahasta.split("/");
        var fecha1 = new Date(parseInt(desde[2], 10), parseInt(desde[1], 10) - 1, desde[0]);
        var fecha2 = new Date(parseInt(hasta[2], 10), parseInt(hasta[1], 10) - 1, hasta[0]);
        if (fecha2 < fecha1)
        {
            alert("La fecha final no puede ser menor que la fecha inicial.");
            $("#fecha_hasta").val("");
            return false;
        }
    }

    function filtrar() {
        var fechadesde = $("#fecha_desde").val();
        var fechahasta = $("#fecha_hasta").val();
        var hora_desde = $("#hora_desde").val();
        var hora_hasta = $("#hora_hasta").val();
        if (fechadesde == "" || fechahasta == "") {
            alert("Complete la seleccion de fechas antes de filtrar la informacion\n\nSi desea consultar un dia, seleccione la misma fecha en ambos campos.");
            return false;
        }
        var desde = hora_desde != "" ? fechadesde + ' ' + hora_desde : fechadesde;
        var hasta = hora_hasta != "" ? fechahasta + ' ' + hora_hasta : fechahasta;
        $("#mensajes").html('<i style="color:red;vertical-align:middle" class="fas fa-cog fa-spin fa-2x"></i><span style="color:red;font-size:14px;margin-left:10px;margin-top:5px;">Buscando información por favor espere...</span>');
        $("#mensajes").show("slow");
        var vehiculos = $("#vehiculos").val();
        var eventos = $("#eventos").val();
        var url = "<?= Url::to(['reportes/historico'], true); ?>";
        window.location.href = url + "?vehiculos=" + vehiculos + "&eventos=" + eventos + "&desde=" + desde + "&hasta=" + hasta;
    }

    function setVehiculos(grupo) {
        if (grupo > 0) {
            $("#mensajes").html('<i style="color:red;vertical-align:middle" class="fas fa-cog fa-spin fa-2x"></i><span style="color:red;font-size:14px;margin-left:10px;margin-top:5px;">Buscando vehiculos del grupo seleccionado...</span>');
            $("#mensajes").show("slow");
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::$app->request->baseUrl . '/site/vehiculos-grupo'; ?>",
                dataType: "html",
                async: true,
                data: {grupo: grupo},
                success: function (response) {

                    var b = response.split(',').map(function (item) {
                        return parseInt(item);
                    });
                    $("#vehiculos").val(b);
                    $("#vehiculos").trigger('change');
                    $("#mensajes").html('');
                    $("#mensajes").hide("slow");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Problemas enviado datos : \n" + xhr.responseText);
                }
            });

        }
    }

<?php
if (isset($_GET['vehiculos']) == true && $_GET['vehiculos'] != "") {
    echo '$("#vehiculos").val([' . $_GET['vehiculos'] . ']);';
}
if (isset($_GET['eventos']) == true && $_GET['eventos'] != "") {
    echo '$("#eventos").val([' . $_GET['eventos'] . ']);';
}

if (isset($_GET['desde']) == true && $_GET['desde'] != "") {
    $dateArray = explode(' ', $_GET['desde']);
    $dateArrayDate = explode('/', $dateArray[0]);
    $fechaDesde = $dateArrayDate[2] . '-' . $dateArrayDate[1] . '-' . $dateArrayDate[0] . 'T' . $dateArray[1] . ':00';
    echo '$("#fecha_desde").val("' . $dateArray[0] . '");';
    echo '$("#hora_desde").val("' . $dateArray[1] . '");';
} else {
    echo '$("#fecha_desde").val("' . date('d/m/Y') . '");';
}
if (isset($_GET['hasta']) == true && $_GET['hasta'] != "") {
    $dateArray = explode(' ', $_GET['hasta']);
    $dateArrayDate = explode('/', $dateArray[0]);
    $fechaHasta = $dateArrayDate[2] . '-' . $dateArrayDate[1] . '-' . $dateArrayDate[0] . 'T' . $dateArray[1] . ':00';
    echo '$("#fecha_hasta").val("' . $dateArray[0] . '");';
    echo '$("#hora_hasta").val("' . $dateArray[1] . '");';
} else {
    echo '$("#fecha_hasta").val("' . date('d/m/Y') . '");';
}
?>

    $("#patente_div, #header_listado").remove();

</script>

<style>
    .summary {
        font-size: 14px;
        text-align: right;
    }

    .pagination {
        font-size: 14px;
    }

    #registros-grid-container thead{
        background-color: lightgray;
    }

    tbody .select2-container .select2-selection {
        height: 100px;
        overflow-y: scroll;
        overflow-x: hidden;
    } 

    .s2-togall-button{
        display:none;
    }

    .bootstrap-timepicker .inline-addon {
        display:none!important;
    }

    .kv-grid-loading {
        position: relative;
        overflow: hidden;
        max-height: 100%;
    }
    
    .kv-grid-loading .kv-loader {
        position: absolute;
        width: 100px !important;
        height: 100px !important;
        top: 20px!important;
        left: 50% !important;
    }
    
    

</style>